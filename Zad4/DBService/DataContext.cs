﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using LinqToDB;
using DBService.Tables;

namespace DBService
{
    public class DataContext : LinqToDB.Data.DataConnection
    {
        public ITable<Order> Orders => GetTable<Order>();
        public ITable<OrderItem> OrderItems => GetTable<OrderItem>();
        public ITable<Product> Products => GetTable<Product>();
        public ITable<Variant> Variants => GetTable<Variant>();
        public ITable<User> Users => GetTable<User>();

        public DataContext() : base("DataContext")
        {
            // EMPTY
        }
    }
}
