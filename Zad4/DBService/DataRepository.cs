﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using NLog;
using LinqToDB;

using DBService.Model;
using DBService.Tables;

namespace DBService
{
    public static class DataRepository
    {
        private static Logger _logger = NLog.LogManager.GetCurrentClassLogger();

        #region -- Variables -------------------------------
        #endregion


        #region -- Constructors ----------------------------
        #endregion


        #region -- Methods ---------------------------------
        #region Orders
        public static IEnumerable<OrderModel> AllOrders()
        {
            try
            {
                _logger.Info("AllOrders()");

                using (var db = new DataContext())
                {
                    var query = from or in db.Orders
                                select new OrderModel
                                {
                                    Id = or.Id,
                                    Date = or.Date,
                                    UserId = or.UserId
                                };
                    return query.ToList();
                }
            }
            catch (Exception e)
            {
                _logger.Error(e);
            }
            return null;
        }
        public static IEnumerable<OrderWithUserModel> AllOrdersWtihUsers()
        {
            try
            {
                _logger.Info("AllOrdersWtihUsers()");

                using (var db = new DataContext())
                {
                    var query = from or in db.Orders
                                join us in db.Users on or.UserId equals us.Id
                                select new OrderWithUserModel
                                {
                                    Id = or.Id,
                                    UserId = or.UserId,
                                    User = us.Name + " " + us.Surname,
                                    Date = or.Date
                                };
                    var orders = query.ToList();

                    foreach (var order in orders)
                    {
                        order.Items.AddRange(AllOrderItemsWithProduct(order.Id));
                    }

                    return orders;
                }
            }
            catch (Exception e)
            {
                _logger.Error(e);
            }
            return null;
        }
        public static OrderModel OrderById(int orderId)
        {
            try
            {
                _logger.Info("OrderById({0})", orderId);

                using (var db = new DataContext())
                {
                    var query = from or in db.Orders
                                where or.Id == orderId
                                select new OrderModel
                                {
                                    Id = or.Id,
                                    Date = or.Date,
                                    UserId = or.UserId
                                };
                    return query.FirstOrDefault();
                }
            }
            catch (Exception e)
            {
                _logger.Error(e);
            }
            return null;
        }
        public static int AddOrder(OrderModel order)
        {
            try
            {
                _logger.Info("AddOrder()");

                using (var db = new DataContext())
                {
                    Order or = new Order()
                    {
                        Id = order.Id,
                        Date = order.Date,
                        UserId = order.UserId
                    };
                    int result = db.InsertWithInt32Identity(or);
                    if (result >= 0)
                        return result;
                }
            }
            catch (Exception e)
            {
                _logger.Error(e);
            }
            return -1;
        }
        public static bool EditOrder(OrderModel order)
        {
            try
            {
                _logger.Info("EditOrder(id={0})", order.Id);

                using (var db = new DataContext())
                {
                    Order or = new Order()
                    {
                        Id = order.Id,
                        Date = order.Date,
                        UserId = order.UserId
                    };
                    int result = db.Update(or);
                    if (result >= 0)
                        return true;
                }
            }
            catch (Exception e)
            {
                _logger.Error(e);
            }
            return false;
        }
        public static bool RemoveOrder(OrderWithUserModel order)
        {
            try
            {
                _logger.Info("RemoveOrder(id={0})", order.Id);

                using (var db = new DataContext())
                {
                    Order or = new Order()
                    {
                        Id = order.Id,
                        Date = order.Date,
                        UserId = order.UserId
                    };
                    int result = db.Delete(or);
                    if (result >= 0)
                        return true;
                }
            }
            catch (Exception e)
            {
                _logger.Error(e);
            }
            return false;
        }
        public static bool RemoveOrder(OrderModel order)
        {
            try
            {
                _logger.Info("RemoveOrder(id={0})", order.Id);

                using (var db = new DataContext())
                {
                    Order or = new Order()
                    {
                        Id = order.Id,
                        Date = order.Date,
                        UserId = order.UserId
                    };
                    int result = db.Delete(or);
                    if (result >= 0)
                        return true;
                }
            }
            catch (Exception e)
            {
                _logger.Error(e);
            }
            return false;
        }
        #endregion

        #region OrderItems
        public static IEnumerable<ItemOrderWithProductModel> AllOrderItemsWithProduct(int orderId)
        {
            try
            {
                _logger.Info("AllOrderItemsWithProduct()");

                using (var db = new DataContext())
                {
                    var query = from or in db.OrderItems
                                where or.OrderId == orderId
                                join vr in db.Variants on or.ProductVariantId equals vr.Id
                                join pr in db.Products on vr.ProductId equals pr.Id
                                select new ItemOrderWithProductModel
                                {
                                    Id = or.Id,
                                    OrderId = or.OrderId,
                                    VariantId = or.ProductVariantId,
                                    ProductId = pr.Id,
                                    ProductManufacture = pr.Manufacture,
                                    ProductName = pr.Name,
                                    Quantity = or.Quantity,
                                    VariantDesc = vr.Desc,
                                    VariantPrice = vr.Price
                                };
                    return query.ToList();
                }
            }
            catch (Exception e)
            {
                _logger.Error(e);
            }
            return null;
        }
        public static ItemOrderWithProductModel OrderItemById(int orderItemId)
        {
            try
            {
                _logger.Info("OrderItemById({0})", orderItemId);

                using (var db = new DataContext())
                {
                    var query = from or in db.OrderItems
                                where or.Id == orderItemId
                                join vr in db.Variants on or.ProductVariantId equals vr.Id into varNull
                                from vr in varNull.DefaultIfEmpty()
                                join pr in db.Products on vr.ProductId equals pr.Id into prodNull
                                from pr in prodNull.DefaultIfEmpty()
                                select new ItemOrderWithProductModel
                                {
                                    Id = or.Id,
                                    OrderId = or.OrderId,
                                    VariantId = or.ProductVariantId,
                                    ProductId = pr.Id,
                                    ProductManufacture = pr.Manufacture,
                                    ProductName = pr.Name,
                                    Quantity = or.Quantity,
                                    VariantDesc = vr.Desc,
                                    VariantPrice = vr.Price
                                };
                    return query.FirstOrDefault();
                }
            }
            catch (Exception e)
            {
                _logger.Error(e);
            }
            return null;
        }
        public static int AddOrderItem(ItemOrderWithProductModel orderItem, int orderId)
        {
            try
            {
                _logger.Info("AddOrderItem()");

                using (var db = new DataContext())
                {
                    OrderItem or = new OrderItem()
                    {
                        Id = orderItem.Id,
                        OrderId = orderId,
                        ProductVariantId = orderItem.VariantId,
                        Quantity = orderItem.Quantity
                    };
                    int result = db.InsertWithInt32Identity(or);
                    if (result >= 0)
                        return result;
                }
            }
            catch (Exception e)
            {
                _logger.Error(e);
            }
            return -1;
        }
        public static bool EditOrderItem(ItemOrderWithProductModel orderItem)
        {
            try
            {
                _logger.Info("EditOrderItem(id={0})", orderItem.Id);

                using (var db = new DataContext())
                {
                    OrderItem or = new OrderItem()
                    {
                        Id = orderItem.Id,
                        OrderId = orderItem.OrderId,
                        ProductVariantId = orderItem.VariantId,
                        Quantity = orderItem.Quantity
                    };
                    int result = db.Update(or);
                    if (result >= 0)
                        return true;
                }
            }
            catch (Exception e)
            {
                _logger.Error(e);
            }
            return false;
        }
        public static bool RemoveOrderItem(ItemOrderWithProductModel orderItem)
        {
            try
            {
                _logger.Info("RemoveOrderItem(id={0})", orderItem.Id);

                using (var db = new DataContext())
                {
                    OrderItem or = new OrderItem()
                    {
                        Id = orderItem.Id,
                        OrderId = orderItem.OrderId,
                        ProductVariantId = orderItem.VariantId,
                        Quantity = orderItem.Quantity
                    };
                    int result = db.Delete(or);
                    if (result >= 0)
                        return true;
                }
            }
            catch (Exception e)
            {
                _logger.Error(e);
            }
            return false;
        }
        #endregion

        #region Users
        public static IEnumerable<UserModel> AllUsers()
        {
            try
            {
                _logger.Info("AllUsers()");

                using (var db = new DataContext())
                {
                    var query = from us in db.Users
                                select new UserModel
                                {
                                    Id = us.Id,
                                    Login = us.Login,
                                    Name = us.Name,
                                    Password = us.Password,
                                    Surname = us.Surname
                                };
                    return query.ToList();
                }
            }
            catch (Exception e)
            {
                _logger.Error(e);
            }
            return null;
        }
        public static UserModel UserById(int userId)
        {
            try
            {
                _logger.Info("UserById({0})", userId);

                using (var db = new DataContext())
                {
                    var query = from us in db.Users
                                where us.Id == userId
                                select new UserModel
                                {
                                    Id = us.Id,
                                    Login = us.Login,
                                    Name = us.Name,
                                    Password = us.Password,
                                    Surname = us.Surname
                                };
                    return query.FirstOrDefault();
                }
            }
            catch (Exception e)
            {
                _logger.Error(e);
            }
            return null;
        }
        public static int AddUser(UserModel user)
        {
            try
            {
                _logger.Info("AddUser()");

                using (var db = new DataContext())
                {
                    User ur = new User()
                    {
                        Id = user.Id,
                        Login = user.Login,
                        Name = user.Name,
                        Surname = user.Surname,
                        Password = user.Password
                    };
                    int result = db.InsertWithInt32Identity(ur);
                    if (result >= 0)
                        return result;
                }
            }
            catch (Exception e)
            {
                _logger.Error(e);
            }
            return -1;
        }
        public static bool EditUser(UserModel user)
        {
            try
            {
                _logger.Info("EditUser(id={0})", user.Id);

                using (var db = new DataContext())
                {
                    User ur = new User()
                    {
                        Id = user.Id,
                        Login = user.Login,
                        Name = user.Name,
                        Surname = user.Surname,
                        Password = user.Password
                    };
                    int result = db.Update(ur);
                    if (result >= 0)
                        return true;
                }
            }
            catch (Exception e)
            {
                _logger.Error(e);
            }
            return false;
        }
        public static bool RemoveUser(UserModel user)
        {
            try
            {
                _logger.Info("RemoveUser(id={0})", user.Id);

                using (var db = new DataContext())
                {
                    User ur = new User()
                    {
                        Id = user.Id,
                        Login = user.Login,
                        Name = user.Name,
                        Surname = user.Surname,
                        Password = user.Password
                    };
                    int result = db.Delete(ur);
                    if (result >= 0)
                        return true;
                }
            }
            catch (Exception e)
            {
                _logger.Error(e);
            }
            return false;
        }
        #endregion

        #region Products
        public static IEnumerable<ProductModel> AllProducts()
        {
            try
            {
                _logger.Info("AllProducts()");

                using (var db = new DataContext())
                {
                    var query = from pr in db.Products
                                select new ProductModel
                                {
                                    Id = pr.Id,
                                    Name = pr.Name,
                                    Manufacture = pr.Manufacture,
                                    Desc = pr.Desc
                                };
                    return query.ToList();
                }
            }
            catch (Exception e)
            {
                _logger.Error(e);
            }
            return null;
        }
        public static ProductModel ProductById(int productId)
        {
            try
            {
                _logger.Info("ProductById({0})", productId);

                using (var db = new DataContext())
                {
                    var query = from pr in db.Products
                                where pr.Id == productId
                                select new ProductModel
                                {
                                    Id = pr.Id,
                                    Name = pr.Name,
                                    Manufacture = pr.Manufacture,
                                    Desc = pr.Desc
                                };
                    return query.FirstOrDefault();
                }
            }
            catch (Exception e)
            {
                _logger.Error(e);
            }
            return null;
        }
        public static int AddProduct(ProductModel product)
        {
            try
            {
                _logger.Info("AddProduct()");

                using (var db = new DataContext())
                {
                    Product pr = new Product()
                    {
                        Id = product.Id,
                        Name = product.Name,
                        Manufacture = product.Manufacture,
                        Desc = product.Desc
                    };
                    int result = db.InsertWithInt32Identity(pr);
                    if (result >= 0)
                        return result;
                }
            }
            catch (Exception e)
            {
                _logger.Error(e);
            }
            return -1;
        }
        public static bool EditProduct(ProductModel product)
        {
            try
            {
                _logger.Info("EditProduct(id={0})", product.Id);

                using (var db = new DataContext())
                {
                    Product pr = new Product()
                    {
                        Id = product.Id,
                        Name = product.Name,
                        Manufacture = product.Manufacture,
                        Desc = product.Desc
                    };
                    int result = db.Update(pr);
                    if (result >= 0)
                        return true;
                }
            }
            catch (Exception e)
            {
                _logger.Error(e);
            }
            return false;
        }
        public static bool RemoveProduct(ProductModel product)
        {
            try
            {
                _logger.Info("RemoveProduct(id={0})", product.Id);

                using (var db = new DataContext())
                {
                    Product pr = new Product()
                    {
                        Id = product.Id,
                        Name = product.Name,
                        Manufacture = product.Manufacture,
                        Desc = product.Desc
                    };
                    int result = db.Delete(pr);
                    if (result >= 0)
                        return true;
                }
            }
            catch (Exception e)
            {
                _logger.Error(e);
            }
            return false;
        }
        #endregion

        #region Variants
        public static IEnumerable<VariantModel> AllVariants()
        {
            try
            {
                _logger.Info("AllVariants()");

                using (var db = new DataContext())
                {
                    var query = from vr in db.Variants
                                select new VariantModel
                                {
                                    Id = vr.Id,
                                    Price = vr.Price,
                                    ProductId = vr.ProductId,
                                    Quantity = vr.Quantity,
                                    Desc = vr.Desc
                                };
                    return query.ToList();
                }
            }
            catch (Exception e)
            {
                _logger.Error(e);
            }
            return null;
        }
        public static IEnumerable<VariantWithProductModel> AllVariantsWithProducts()
        {
            try
            {
                _logger.Info("AllVariantsWithProducts()");

                using (var db = new DataContext())
                {
                    var query = from vr in db.Variants
                                join pr in db.Products on vr.ProductId equals pr.Id
                                select new VariantWithProductModel
                                {
                                    Id = vr.Id,
                                    ProductId = vr.ProductId,
                                    ProductName = pr.Name,
                                    ProductManufacture = pr.Manufacture,
                                    Desc = vr.Desc,
                                    Quantity = vr.Quantity,
                                    Price = vr.Price
                                };
                    return query.ToList();
                }
            }
            catch (Exception e)
            {
                _logger.Error(e);
            }
            return null;
        }
        public static VariantModel VariantById(int variantId)
        {
            try
            {
                _logger.Info("VariantById({0})", variantId);

                using (var db = new DataContext())
                {
                    var query = from vr in db.Variants
                                where vr.Id == variantId
                                select new VariantModel
                                {
                                    Id = vr.Id,
                                    Price = vr.Price,
                                    ProductId = vr.ProductId,
                                    Quantity = vr.Quantity,
                                    Desc = vr.Desc
                                };
                    return query.FirstOrDefault();
                }
            }
            catch (Exception e)
            {
                _logger.Error(e);
            }
            return null;
        }
        public static int AddVariant(VariantModel variant)
        {
            try
            {
                _logger.Info("AddVariant()");

                using (var db = new DataContext())
                {
                    Variant vr = new Variant()
                    {
                        Id = variant.Id,
                        Price = variant.Price,
                        Quantity = variant.Quantity,
                        ProductId = variant.ProductId,
                        Desc = variant.Desc
                    };
                    int result = db.InsertWithInt32Identity(vr);
                    if (result >= 0)
                        return result;
                }
            }
            catch (Exception e)
            {
                _logger.Error(e);
            }
            return -1;
        }
        public static bool EditVariant(VariantModel variant)
        {
            try
            {
                _logger.Info("EditVariant(id={0})", variant.Id);

                using (var db = new DataContext())
                {
                    Variant vr = new Variant()
                    {
                        Id = variant.Id,
                        Price = variant.Price,
                        Quantity = variant.Quantity,
                        ProductId = variant.ProductId,
                        Desc = variant.Desc
                    };
                    int result = db.Update(vr);
                    if (result >= 0)
                        return true;
                }
            }
            catch (Exception e)
            {
                _logger.Error(e);
            }
            return false;
        }
        public static bool RemoveVariant(VariantModel variant)
        {
            try
            {
                _logger.Info("EditVariant(id={0})", variant.Id);

                using (var db = new DataContext())
                {
                    Variant vr = new Variant()
                    {
                        Id = variant.Id,
                        Price = variant.Price,
                        Quantity = variant.Quantity,
                        ProductId = variant.ProductId,
                        Desc = variant.Desc
                    };
                    int result = db.Delete(vr);
                    if (result >= 0)
                        return true;
                }
            }
            catch (Exception e)
            {
                _logger.Error(e);
            }
            return false;
        }
        #endregion
        #endregion
    }
}
