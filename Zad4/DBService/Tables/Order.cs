﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using LinqToDB;
using LinqToDB.Mapping;

namespace DBService.Tables
{
    [Table("orders")]
    public class Order
    {
        [Column("order_id"), PrimaryKey, Identity]
        public int Id { get; set; } // int(11)

        [Column("user_id"), NotNull]
        public int UserId { get; set; } // int

        [Column("date"), NotNull]
        public DateTime Date { get; set; } // text
    }
}
