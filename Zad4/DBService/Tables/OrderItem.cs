﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using LinqToDB;
using LinqToDB.Mapping;

namespace DBService.Tables
{
    [Table("order_items")]
    public class OrderItem
    {
        [Column("item_id"), PrimaryKey, Identity]
        public int Id { get; set; } // int(11)

        [Column("order_id"), NotNull]
        public int OrderId { get; set; } // int(11)

        [Column("product_variant_id"), NotNull]
        public int ProductVariantId { get; set; } // int(11)

        [Column("quantity"), NotNull]
        public double Quantity { get; set; } // double
    }
}
