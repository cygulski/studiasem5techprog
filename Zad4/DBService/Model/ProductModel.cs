﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBService.Model
{
    public class ProductModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Desc { get; set; }
        public string Manufacture { get; set; }

        public override string ToString()
        {
            return string.Format("[{0}] {1} : {2}", Id, Manufacture, Name);
        }
    }
}
