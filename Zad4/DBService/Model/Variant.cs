﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using LinqToDB;
using LinqToDB.Mapping;

namespace DBService.Model
{
    [Table("product_variants")]
    public class Variant
    {
        [Column("variant_id"), PrimaryKey, Identity]
        public int Id { get; set; } // int(10)

        [Column("quantity"), NotNull]
        public double Quantity { get; set; } // double

        [Column("price"), NotNull]
        public double Price { get; set; } // double

        [Column("desc"), NotNull]
        public string Desc { get; set; } // text

        [Column("product_id"), NotNull]
        public int ProductId { get; set; } // int(11)
    }
}
