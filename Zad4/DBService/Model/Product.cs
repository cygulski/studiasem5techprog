﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using LinqToDB;
using LinqToDB.Mapping;

namespace DBService.Model
{
    [Table("products")]
    public class Product
    {
        [Column("product_id"), PrimaryKey, Identity]
        public int Id { get; set; } // int(11)

        [Column("name"), NotNull]
        public string Name { get; set; } // text

        [Column("desc"), NotNull]
        public string Desc { get; set; } // text

        [Column("manufacture"), NotNull]
        public string Manufacture { get; set; } // text
    }
}
