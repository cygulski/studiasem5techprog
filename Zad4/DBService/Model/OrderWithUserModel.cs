﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBService.Model
{
    public class OrderWithUserModel
    {
        public int Id { get; set; }
        public string User { get; set; }
        public int UserId { get; set; }
        public DateTime Date { get; set; }

        public List<ItemOrderWithProductModel> Items { get; set; } = new List<ItemOrderWithProductModel>();

        public override string ToString()
        {
            return string.Format("[{0}] {1} ({2})", Id, User, Date);
        }
    }
}
