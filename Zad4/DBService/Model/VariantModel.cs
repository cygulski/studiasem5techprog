﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBService.Model
{
    public class VariantModel
    {
        public int Id { get; set; }
        public double Quantity { get; set; }
        public double Price { get; set; }
        public string Desc { get; set; }
        public int ProductId { get; set; }
        
        public override string ToString()
        {
            return string.Format("[{0}] {1}", Id, Desc);
        }
    }
}
