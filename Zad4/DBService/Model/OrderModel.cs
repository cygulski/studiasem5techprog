﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBService.Model
{
    public class OrderModel
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public DateTime Date { get; set; }

        public override string ToString()
        {
            return string.Format("[{0}] {1}, {2}", Id, UserId, Date.ToShortDateString());
        }
    }
}
