﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBService.Model
{
    public class VariantWithProduct
    {
        public int Id { get; set; }

        public int ProductId { get; set; }
        
        public string ProductName { get; set; }
        
        public string ProductManufacture { get; set; }

        public double Quantity { get; set; }
        
        public double Price { get; set; }
        
        public string Desc { get; set; }
        
    }
}
