﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using LinqToDB;
using LinqToDB.Mapping;

namespace DBService.Model
{
    [Table("users")]
    public class User
    {
        [Column("user_id"), PrimaryKey, Identity]
        public int Id { get; set; } // int(11)

        [Column("login"), NotNull]
        public string Login { get; set; } // varchar(10)

        [Column("password"), NotNull]
        public string Password { get; set; } // text

        [Column("first_name"), NotNull]
        public string Name { get; set; } // text

        [Column("last_name"), NotNull]
        public string Surname { get; set; } // text
    }
}
