﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBService.Model
{
    public class ItemOrderWithProductModel
    {
        public int Id { get; set; }
        public int OrderId { get; set; }
        public int VariantId { get; set; }
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public string ProductManufacture { get; set; }
        public string VariantDesc { get; set; }
        public double VariantPrice { get; set; }
        public double Quantity { get; set; }
        
        public override string ToString()
        {
            return string.Format("[{0}] {1} : {2} : ", Id, ProductName, ProductManufacture, VariantDesc);
        }
    }
}
