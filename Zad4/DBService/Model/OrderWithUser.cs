﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBService.Model
{
    public class OrderWithUser
    {
        public int Id { get; set; }
        public string User { get; set; }
        public int UserId { get; set; }
        public string Date { get; set; }
    }
}
