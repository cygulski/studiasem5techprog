﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.ComponentModel;
using System.Collections.ObjectModel;

using NLog;

using WpfApp.Services;
using DBService;
using DBService.Model;
using WpfApp.Helpers;
using System.Windows.Threading;
using System.Threading;

namespace WpfApp.ModelView
{
    /// <summary>
    /// ModelView for OrdersView.xaml
    /// </summary>
    public class OrdersModelView : INotifyPropertyChanged
    {
        private Logger _logger = NLog.LogManager.GetCurrentClassLogger();

        #region -- Variables -------------------------------
        private ICommand _remove = null;
        private ICommand _add = null;
        private ICommand _edit = null;

        private ICommand _products = null;
        private ICommand _users = null;
        private ICommand _variants = null;

        private ObservableCollection<OrderWithUserModel> _ordersList = new ObservableCollection<OrderWithUserModel>();
        private OrderWithUserModel _selectedOrder = null;
        #endregion

        #region -- Events ----------------------------------
        public event PropertyChangedEventHandler PropertyChanged;
        public event EventHandler DataLoaded;
        #endregion

        #region -- Properties ------------------------------
        public ObservableCollection<OrderWithUserModel> OrdersList
        {
            get
            {
                return _ordersList;
            }
            set
            {
                _ordersList = value;
                NotifyPropertyChanged("OrdersList");
            }
        }
        public OrderWithUserModel SelectedOrder
        {
            get
            {
                return _selectedOrder;
            }
            set
            {
                _selectedOrder = value;
                NotifyPropertyChanged("SelectedOrder");
            }
        }
        #endregion

        #region -- Constructors ----------------------------
        public OrdersModelView()
        {
            ReloadOrders();
        }
        #endregion

        #region -- Methods ---------------------------------
        private void NotifyPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        public void ReloadOrders()
        {
            OrdersList.Clear();
            SelectedOrder = null;

            /*IEnumerable<OrderWithUserModel> orders = DataRepository.AllOrdersWtihUsers();
            foreach (OrderWithUserModel or in orders)
            {
                OrdersList.Add(or);
            }*/
            
            var dispatcher = Dispatcher.CurrentDispatcher;
            Task<IEnumerable<OrderWithUserModel>> task = Task<IEnumerable<OrderWithUserModel>>.Factory.StartNew(() =>
            {
                return DataRepository.AllOrdersWtihUsers();
            });
            Task UITask = task.ContinueWith(x =>
            {
                dispatcher.Invoke(new Action(() =>
                {
                    foreach (OrderWithUserModel or in x.Result)
                    {
                        OrdersList.Add(or);
                    }

                    DataLoaded?.Invoke(this, EventArgs.Empty);
                }));
            });
        }
        #endregion

        #region -- ICommand --------------------------------
        public ICommand Remove
        {
            get
            {
                try
                {
                    if (_remove == null)
                    {
                        _remove = new RelayCommand(s =>
                        {
                            if (DataRepository.RemoveOrder(SelectedOrder))
                            {
                                ReloadOrders();
                            }
                        });
                    }
                }
                catch (Exception e)
                {
                    _logger.Error(e);
                }

                return _remove;
            }
        }
        public ICommand Add
        {
            get
            {
                try
                {
                    if (_add == null)
                    {
                        _add = new RelayCommand(s =>
                        {
                            EditOrderModelView mv = new EditOrderModelView(Mode.Add, -1);
                            ViewsManager.ShowWindowDialog(mv);

                            ReloadOrders();
                        });
                    }
                }
                catch (Exception e)
                {
                    _logger.Error(e);
                }

                return _add;
            }
        }
        public ICommand Edit
        {
            get
            {
                try
                {
                    if (_edit == null)
                    {
                        _edit = new RelayCommand(s =>
                        {
                            EditOrderModelView mv = new EditOrderModelView(Mode.Edit, SelectedOrder.Id);
                            ViewsManager.ShowWindowDialog(mv);

                            ReloadOrders();
                        });
                    }
                }
                catch (Exception e)
                {
                    _logger.Error(e);
                }

                return _edit;
            }
        }
        
        public ICommand Products
        {
            get
            {
                try
                {
                    if (_products == null)
                    {
                        _products = new RelayCommand(s =>
                        {
                            ProductsModelView mv = new ProductsModelView();
                            ViewsManager.ShowWindowDialog(mv);

                            ReloadOrders();
                        });
                    }
                }
                catch (Exception e)
                {
                    _logger.Error(e);
                }

                return _products;
            }
        }
        public ICommand Users
        {
            get
            {
                try
                {
                    if (_users == null)
                    {
                        _users = new RelayCommand(s =>
                        {
                            UsersModelView mv = new UsersModelView();
                            ViewsManager.ShowWindowDialog(mv);

                            ReloadOrders();
                        });
                    }
                }
                catch (Exception e)
                {
                    _logger.Error(e);
                }

                return _users;
            }
        }
        public ICommand Variants
        {
            get
            {
                try
                {
                    if (_variants == null)
                    {
                        _variants = new RelayCommand(s =>
                        {
                            VariantsModelView mv = new VariantsModelView();
                            ViewsManager.ShowWindowDialog(mv);

                            ReloadOrders();
                        });
                    }
                }
                catch (Exception e)
                {
                    _logger.Error(e);
                }

                return _variants;
            }
        }
        #endregion
    }
}
