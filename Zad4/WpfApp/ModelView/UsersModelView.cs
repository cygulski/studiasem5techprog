﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.ComponentModel;

using NLog;

using WpfApp.Services;
using DBService;
using DBService.Model;
using WpfApp.Helpers;
using System.Windows.Threading;
using System.Threading;

namespace WpfApp.ModelView
{
    /// <summary>
    /// ModelView for UsersView.xaml
    /// </summary>
    public class UsersModelView : INotifyPropertyChanged
    {
        private Logger _logger = NLog.LogManager.GetCurrentClassLogger();

        #region -- Variables -------------------------------
        private ICommand _remove = null;
        private ICommand _add = null;
        private ICommand _edit = null;

        private ObservableCollection<UserModel> _usersList = new ObservableCollection<UserModel>();
        private UserModel _selectedUser = null;
        #endregion

        #region -- Events ----------------------------------
        public event PropertyChangedEventHandler PropertyChanged;
        public event EventHandler DataLoaded;
        #endregion

        #region -- Properties ------------------------------
        public ObservableCollection<UserModel> UsersList
        {
            get
            {
                return _usersList;
            }
            set
            {
                _usersList = value;
                NotifyPropertyChanged("UsersList");
            }
        }
        public UserModel SelectedUser
        {
            get
            {
                return _selectedUser;
            }
            set
            {
                _selectedUser = value;
                NotifyPropertyChanged("SelectedUser");
            }
        }
        #endregion

        #region -- Constructors ----------------------------
        public UsersModelView()
        {
            ReloadUsers();
        }
        #endregion

        #region -- Methods ---------------------------------
        private void NotifyPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        public void ReloadUsers()
        {
            UsersList.Clear();
            SelectedUser = null;

            /*IEnumerable<UserModel> users = DataRepository.AllUsers();
            foreach (UserModel us in users)
            {
                UsersList.Add(us);
            }*/

            var dispatcher = Dispatcher.CurrentDispatcher;
            Task<IEnumerable<UserModel>> task = Task<IEnumerable<UserModel>>.Factory.StartNew(() =>
            {
                return DataRepository.AllUsers();
            });
            Task UITask = task.ContinueWith(x =>
            {
                dispatcher.Invoke(new Action(() =>
                {
                    foreach (UserModel or in x.Result)
                    {
                        UsersList.Add(or);
                    }

                    DataLoaded?.Invoke(this, EventArgs.Empty);
                }));
            });
        }
        #endregion

        #region -- ICommand --------------------------------
        public ICommand Remove
        {
            get
            {
                try
                {
                    if (_remove == null)
                    {
                        _remove = new RelayCommand(s =>
                        {
                            if (DataRepository.RemoveUser(SelectedUser))
                            {
                                ReloadUsers();
                            }
                        });
                    }
                }
                catch (Exception e)
                {
                    _logger.Error(e);
                }

                return _remove;
            }
        }
        public ICommand Add
        {
            get
            {
                try
                {
                    if (_add == null)
                    {
                        _add = new RelayCommand(s =>
                        {
                            EditUserModelView mv = new EditUserModelView(Mode.Add, -1);
                            ViewsManager.ShowWindowDialog(mv);

                            ReloadUsers();
                        });
                    }
                }
                catch (Exception e)
                {
                    _logger.Error(e);
                }

                return _add;
            }
        }
        public ICommand Edit
        {
            get
            {
                try
                {
                    if (_edit == null)
                    {
                        _edit = new RelayCommand(s =>
                        {
                            EditUserModelView mv = new EditUserModelView(Mode.Edit, SelectedUser.Id);
                            ViewsManager.ShowWindowDialog(mv);

                            ReloadUsers();
                        });
                    }
                }
                catch (Exception e)
                {
                    _logger.Error(e);
                }

                return _edit;
            }
        }
        #endregion
    }
}
