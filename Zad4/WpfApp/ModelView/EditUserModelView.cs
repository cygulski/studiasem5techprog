﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.ComponentModel;
using System.Collections.ObjectModel;

using NLog;

using WpfApp.Helpers;
using WpfApp.Services;
using DBService.Model;
using DBService;

namespace WpfApp.ModelView
{
    /// <summary>
    /// ModelView for EditUserView.xaml
    /// </summary>
    public class EditUserModelView : INotifyPropertyChanged
    {
        private Logger _logger = NLog.LogManager.GetCurrentClassLogger();

        #region -- Variables -------------------------------
        private ICommand _save = null;
        private ICommand _cancel = null;

        private Mode _mode = Mode.Add;

        private string _title = "New user";
        private string _buttonText = "Add";

        private int _id = -1;
        private string _login = "";
        private string _password = "";
        private string _name = "";
        private string _surname = "";
        #endregion

        #region -- Events ----------------------------------
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

        #region -- Properties ------------------------------
        public string Title
        {
            get { return _title; }
            set
            {
                _title = value;
                NotifyPropertyChanged("Title");
            }
        }

        public string ButtonText
        {
            get { return _buttonText; }
            set
            {
                _buttonText = value;
                NotifyPropertyChanged("ButtonText");
            }
        }

        public int Id
        {
            get { return _id; }
            set
            {
                _id = value;
                NotifyPropertyChanged("Id");
            }
        }
        public string Login
        {
            get { return _login; }
            set
            {
                _login = value;
                NotifyPropertyChanged("Login");
            }
        }
        public string Password
        {
            get { return _password; }
            set
            {
                _password = value;
                NotifyPropertyChanged("Password");
            }
        }
        public string Name
        {
            get { return _name; }
            set
            {
                _name = value;
                NotifyPropertyChanged("Name");
            }
        }
        public string Surname
        {
            get { return _surname; }
            set
            {
                _surname = value;
                NotifyPropertyChanged("Surname");
            }
        }
        #endregion

        #region -- Constructors ----------------------------
        public EditUserModelView()
        {

        }

        public EditUserModelView(Mode mode, int id) : this()
        {
            _mode = mode;
            if (mode == Mode.Edit)
            {
                Title = "Edit user - " + id;
                ButtonText = "Save";
                ReloadUser(id);
            }

        }
        #endregion

        #region -- Methods ---------------------------------
        private void NotifyPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        private void ReloadUser(int id)
        {
            if (id >= 0)
            {
                UserModel user = DataRepository.UserById(id);
                
                if (user != null)
                {
                    Id = user.Id;
                    Login = user.Login;
                    Password = user.Password;
                    Name = user.Name;
                    Surname = user.Surname;
                }
            }
        }
        #endregion

        #region -- ICommand --------------------------------
        public ICommand Save
        {
            get
            {
                try
                {
                    if (_save == null)
                    {
                        _save = new RelayCommand(s =>
                        {
                            UserModel newUser = new UserModel()
                            {
                                Id = Id,
                                Login = Login,
                                Name = Name,
                                Password = Password,
                                Surname = Surname
                            };

                            if (_mode == Mode.Add)
                            {
                                int result = DataRepository.AddUser(newUser);
                            }
                            else if (_mode == Mode.Edit)
                            {
                                bool result = DataRepository.EditUser(newUser); 
                            }
                        });
                    }
                }
                catch (Exception e)
                {
                    _logger.Error(e);
                }

                return _save;
            }
        }
        public ICommand Cancel
        {
            get
            {
                try
                {
                    if (_cancel == null)
                    {
                        _cancel = new RelayCommand(s =>
                        {
                            ViewsManager.CloseWindowDialog(this);
                        });
                    }
                }
                catch (Exception e)
                {
                    _logger.Error(e);
                }

                return _cancel;
            }
        }
        #endregion
    }
}
