﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.ComponentModel;
using System.Collections.ObjectModel;

using NLog;

using WpfApp.Helpers;
using WpfApp.Services;
using DBService.Model;
using DBService;

namespace WpfApp.ModelView
{
    /// <summary>
    /// ModelView for EditOrderView.xaml
    /// </summary>
    public class EditOrderModelView : INotifyPropertyChanged
    {
        private Logger _logger = NLog.LogManager.GetCurrentClassLogger();

        #region -- Variables -------------------------------
        private Mode _mode = Mode.Add;

        private ICommand _save = null;
        private ICommand _cancel = null;

        private ICommand _add = null;
        private ICommand _remove = null;
        
        private ObservableCollection<UserModel> _usersList = new ObservableCollection<UserModel>();
        private UserModel _selectedUser = null;
        private ObservableCollection<VariantWithProductModel> _variantsList = new ObservableCollection<VariantWithProductModel>();
        private VariantWithProductModel _selectedVariant = null;
        private ObservableCollection<ItemOrderWithProductModel> _itemsList = new ObservableCollection<ItemOrderWithProductModel>();
        private ObservableCollection<ItemOrderWithProductModel> _removedItemsList = new ObservableCollection<ItemOrderWithProductModel>();
        private ItemOrderWithProductModel _selectedItem = null;

        private string _title = "New order";
        private string _buttonText = "Add";

        private int _id = -1;
        private DateTime _date = DateTime.Now;
        #endregion

        #region -- Events ----------------------------------
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

        #region -- Properties ------------------------------
        public string Title
        {
            get { return _title; }
            set
            {
                _title = value;
                NotifyPropertyChanged("Title");
            }
        }
        public string ButtonText
        {
            get { return _buttonText; }
            set
            {
                _buttonText = value;
                NotifyPropertyChanged("ButtonText");
            }
        }

        public int Id
        {
            get { return _id; }
            set
            {
                _id = value;
                NotifyPropertyChanged("Id");
            }
        }
        public DateTime Date
        {
            get { return _date; }
            set
            {
                _date = value;
                NotifyPropertyChanged("Date");
            }
        }

        public ObservableCollection<UserModel> Users
        {
            get
            {
                return _usersList;
            }
            set
            {
                _usersList = value;
                NotifyPropertyChanged("UsersList");
            }
        }
        public UserModel SelectedUser
        {
            get
            {
                return _selectedUser;
            }
            set
            {
                _selectedUser = value;
                NotifyPropertyChanged("SelectedUsers");
            }
        }
        public ObservableCollection<VariantWithProductModel> Variants
        {
            get
            {
                return _variantsList;
            }
            set
            {
                _variantsList = value;
                NotifyPropertyChanged("VariantsList");
            }
        }
        public VariantWithProductModel SelectedVariant
        {
            get
            {
                return _selectedVariant;
            }
            set
            {
                _selectedVariant = value;
                NotifyPropertyChanged("SelectedVariant");
            }
        }
        public ObservableCollection<ItemOrderWithProductModel> Items
        {
            get
            {
                return _itemsList;
            }
            set
            {
                _itemsList = value;
                NotifyPropertyChanged("Items");
            }
        }
        public ItemOrderWithProductModel SelectedItem
        {
            get
            {
                return _selectedItem;
            }
            set
            {
                _selectedItem = value;
                NotifyPropertyChanged("SelectedItem");
            }
        }
        #endregion

        #region -- Constructors ----------------------------
        public EditOrderModelView()
        {
            ReloadUsers();
            ReloadVariants();
        }
        public EditOrderModelView(Mode mode, int id) : this()
        {
            _mode = mode;
            if (mode == Mode.Edit)
            {
                Title = "Edit order - " + id;
                ButtonText = "Save";

                ReloadOrder(id);
                ReloadItems(id);
            }
        }
        #endregion

        #region -- Methods ---------------------------------
        private void NotifyPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        private void ReloadOrder(int id)
        {
            if (id >= 0)
            {
                OrderModel order = DataRepository.OrderById(id);

                if (order != null)
                {
                    Id = id;
                    Date = order.Date;
                    SelectedUser = Users.FirstOrDefault(x => x.Id == order.UserId);
                }
            }
        }
        private void ReloadUsers()
        {
            Users.Clear();
            SelectedUser = null;

            IEnumerable<UserModel> users = DataRepository.AllUsers();
            foreach (UserModel us in users)
            {
                Users.Add(us);
            }
        }
        private void ReloadVariants()
        {
            Variants.Clear();
            SelectedVariant = null;

            IEnumerable<VariantWithProductModel> variants = DataRepository.AllVariantsWithProducts();
            foreach (VariantWithProductModel vr in variants)
            {
                Variants.Add(vr);
            }
        }
        private void ReloadItems(int id)
        {
            if (id >= 0)
            {
                Items.Clear();
                SelectedItem = null;

                IEnumerable<ItemOrderWithProductModel> items = DataRepository.AllOrderItemsWithProduct(id);
                foreach (ItemOrderWithProductModel it in items)
                {
                    Items.Add(it);
                }
            }
        }
        #endregion

        #region -- ICommand --------------------------------
        public ICommand Save
        {
            get
            {
                try
                {
                    if (_save == null)
                    {
                        _save = new RelayCommand(s =>
                        {
                            OrderModel order = new OrderModel()
                            {
                                Id = Id,
                                Date = Date,
                                UserId = SelectedUser.Id
                            };

                            if (_mode == Mode.Add)
                            {
                                int orderId = DataRepository.AddOrder(order);

                                if (orderId >= 0)
                                {
                                    foreach (ItemOrderWithProductModel item in Items)
                                    {
                                        DataRepository.AddOrderItem(item, orderId);
                                    }
                                }
                            }
                            else
                            {
                                if (DataRepository.EditOrder(order))
                                {
                                    foreach (ItemOrderWithProductModel item in Items)
                                    {
                                        if (item.Id == -1)
                                        {
                                            DataRepository.AddOrderItem(item, order.Id);
                                        }
                                        else
                                        {
                                            DataRepository.EditOrderItem(item);
                                        }
                                    }

                                    foreach (ItemOrderWithProductModel item in _removedItemsList)
                                    {
                                        DataRepository.RemoveOrderItem(item);
                                    }
                                }
                            }
                        });
                    }
                }
                catch (Exception e)
                {
                    _logger.Error(e);
                }

                return _save;
            }
        }
        public ICommand Cancel
        {
            get
            {
                try
                {
                    if (_cancel == null)
                    {
                        _cancel = new RelayCommand(s =>
                        {
                            ViewsManager.CloseWindowDialog(this);
                        });
                    }
                }
                catch (Exception e)
                {
                    _logger.Error(e);
                }

                return _cancel;
            }
        }

        public ICommand Add
        {
            get
            {
                try
                {
                    if (_add == null)
                    {
                        _add = new RelayCommand(s =>
                        {
                            ItemOrderWithProductModel item = new ItemOrderWithProductModel()
                            {
                                Id = -1,
                                OrderId = -1,
                                Quantity = 1,
                                ProductId = SelectedVariant.ProductId,
                                ProductManufacture = SelectedVariant.ProductManufacture,
                                ProductName = SelectedVariant.ProductName,
                                VariantDesc = SelectedVariant.Desc,
                                VariantId = SelectedVariant.Id,
                                VariantPrice = SelectedVariant.Price
                            };

                            Items.Add(item);
                        });
                    }
                }
                catch (Exception e)
                {
                    _logger.Error(e);
                }

                return _add;
            }
        }
        public ICommand Remove
        {
            get
            {
                try
                {
                    if (_remove == null)
                    {
                        _remove = new RelayCommand(s =>
                        {
                            if (SelectedItem != null)
                            {
                                if (SelectedItem.Id >= 0)
                                    _removedItemsList.Add(SelectedItem);
                                Items.Remove(SelectedItem);
                                SelectedItem = null;
                            }
                        });
                    }
                }
                catch (Exception e)
                {
                    _logger.Error(e);
                }

                return _remove;
            }
        }
        #endregion
    }
}
