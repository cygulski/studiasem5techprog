﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.ComponentModel;
using System.Collections.ObjectModel;

using NLog;

using WpfApp.Services;
using DBService;
using DBService.Model;
using WpfApp.Helpers;
using System.Threading;
using System.Windows.Threading;

namespace WpfApp.ModelView
{
    /// <summary>
    /// ModelView for VariantsView.xaml
    /// </summary>
    public class VariantsModelView : INotifyPropertyChanged
    {
        private Logger _logger = NLog.LogManager.GetCurrentClassLogger();

        #region -- Variables -------------------------------
        private ICommand _remove = null;
        private ICommand _add = null;
        private ICommand _edit = null;

        private ObservableCollection<VariantWithProductModel> _variantsList = new ObservableCollection<VariantWithProductModel>();
        private VariantWithProductModel _selectedVariant = null;
        #endregion

        #region -- Events ----------------------------------
        public event PropertyChangedEventHandler PropertyChanged;
        public event EventHandler DataLoaded;
        #endregion

        #region -- Properties ------------------------------
        public ObservableCollection<VariantWithProductModel> VariantsList
        {
            get
            {
                return _variantsList;
            }
            set
            {
                _variantsList = value;
                NotifyPropertyChanged("VariantsList");
            }
        }
        public VariantWithProductModel SelectedVariant
        {
            get
            {
                return _selectedVariant;
            }
            set
            {
                _selectedVariant = value;
                NotifyPropertyChanged("SelectedVariant");
            }
        }
        #endregion

        #region -- Constructors ----------------------------
        public VariantsModelView()
        {
            ReloadVariants();
        }
        #endregion

        #region -- Methods ---------------------------------
        private void NotifyPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        public void ReloadVariants()
        {
            VariantsList.Clear();
            SelectedVariant = null;

            /*IEnumerable<VariantWithProductModel> variants = DataRepository.AllVariantsWithProducts().Result;
            foreach (VariantWithProductModel vr in variants)
            {
                VariantsList.Add(vr);
            }*/

            var dispatcher = Dispatcher.CurrentDispatcher;
            Task<IEnumerable<VariantWithProductModel>> task = Task<IEnumerable<VariantWithProductModel>>.Factory.StartNew(() =>
            {
                return DataRepository.AllVariantsWithProducts();
            });
            Task UITask = task.ContinueWith(x =>
            {
                dispatcher.Invoke(new Action(() =>
                {
                    foreach (VariantWithProductModel vr in x.Result)
                    {
                        VariantsList.Add(vr);
                    }
                    DataLoaded?.Invoke(this, EventArgs.Empty);
                }));
            });
        }
        #endregion

        #region -- ICommand --------------------------------
        public ICommand Remove
        {
            get
            {
                try
                {
                    if (_remove == null)
                    {
                        _remove = new RelayCommand(s =>
                        {
                            VariantModel variant = new VariantModel()
                            {
                                Id = SelectedVariant.Id
                            };

                            if (DataRepository.RemoveVariant(variant))
                            {
                                ReloadVariants();
                            }
                        });
                    }
                }
                catch (Exception e)
                {
                    _logger.Error(e);
                }

                return _remove;
            }
        }
        public ICommand Add
        {
            get
            {
                try
                {
                    if (_add == null)
                    {
                        _add = new RelayCommand(s =>
                        {
                            EditVariantModelView mv = new EditVariantModelView(Mode.Add, -1);
                            ViewsManager.ShowWindowDialog(mv);

                            ReloadVariants();
                        });
                    }
                }
                catch (Exception e)
                {
                    _logger.Error(e);
                }

                return _add;
            }
        }
        public ICommand Edit
        {
            get
            {
                try
                {
                    if (_edit == null)
                    {
                        _edit = new RelayCommand(s =>
                        {
                            EditVariantModelView mv = new EditVariantModelView(Mode.Edit, SelectedVariant.Id);
                            ViewsManager.ShowWindowDialog(mv);

                            ReloadVariants();
                        });
                    }
                }
                catch (Exception e)
                {
                    _logger.Error(e);
                }

                return _edit;
            }
        }
        #endregion
    }
}
