﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.ComponentModel;
using System.Collections.ObjectModel;

using NLog;

using WpfApp.Helpers;
using WpfApp.Services;
using DBService.Model;
using DBService;
using System.Windows.Threading;
using System.Threading;

namespace WpfApp.ModelView
{
    /// <summary>
    /// ModelView for EditVariantView.xaml
    /// </summary>
    public class EditVariantModelView : INotifyPropertyChanged
    {
        private Logger _logger = NLog.LogManager.GetCurrentClassLogger();

        #region -- Variables -------------------------------
        private ICommand _save = null;
        private ICommand _cancel = null;

        private Mode _mode = Mode.Add;

        private string _title = "New product variant";
        private string _buttonText = "Add";
        
        private ObservableCollection<ProductModel> _productsList = new ObservableCollection<ProductModel>();
        private ProductModel _selectedProduct = null;

        private int _id = -1;
        private double _quantity = 0.0;
        private double _price = 0.0;
        private string _description = "";
        #endregion

        #region -- Events ----------------------------------
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

        #region -- Properties ------------------------------
        public string Title
        {
            get { return _title; }
            set
            {
                _title = value;
                NotifyPropertyChanged("Title");
            }
        }

        public string ButtonText
        {
            get { return _buttonText; }
            set
            {
                _buttonText = value;
                NotifyPropertyChanged("ButtonText");
            }
        }
        
        public ObservableCollection<ProductModel> ProductsList
        {
            get
            {
                return _productsList;
            }
            set
            {
                _productsList = value;
                NotifyPropertyChanged("ProductsList");
            }
        }
        public ProductModel SelectedProduct
        {
            get
            {
                return _selectedProduct;
            }
            set
            {
                _selectedProduct = value;
                NotifyPropertyChanged("SelectedProduct");
            }
        }

        public int Id
        {
            get { return _id; }
            set
            {
                _id = value;
                NotifyPropertyChanged("Id");
            }
        }
        public double Quantity
        {
            get { return _quantity; }
            set
            {
                _quantity = value;
                NotifyPropertyChanged("Quantity");
            }
        }
        public double Price
        {
            get { return _price; }
            set
            {
                _price = value;
                NotifyPropertyChanged("Price");
            }
        }
        public string Description
        {
            get { return _description; }
            set
            {
                _description = value;
                NotifyPropertyChanged("Description");
            }
        }
        #endregion

        #region -- Constructors ----------------------------
        public EditVariantModelView()
        {
            ReloadProducts();
        }

        public EditVariantModelView(Mode mode, int id) : this()
        {
            _mode = mode;
            
            if (mode == Mode.Edit)
            {
                Title = "Edit product variant - " + id;
                ButtonText = "Save";
                ReloadVariant(id);
            }
        }
        #endregion

        #region -- Methods ---------------------------------
        private void NotifyPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
            //_save?.RaiseCanExecuteChanged();
        }

        private void ReloadVariant(int id)
        {
            if (id >= 0)
            {
                VariantModel variant = DataRepository.VariantById(id);

                if (variant != null)
                {
                    Id = variant.Id;
                    SelectedProduct = ProductsList.FirstOrDefault(x => x.Id == variant.ProductId);
                    Quantity = variant.Quantity;
                    Price = variant.Price;
                    Description = variant.Desc;
                }
            }
        }
        private void ReloadProducts()
        {
            ProductsList.Clear();
            SelectedProduct = null;

            IEnumerable<ProductModel> products = DataRepository.AllProducts();
            foreach (ProductModel pr in products)
            {
                ProductsList.Add(pr);
            }
        }
        #endregion

        #region -- ICommand --------------------------------
        public ICommand Save
        {
            get
            {
                try
                {
                    if (_save == null)
                    {
                        _save = new RelayCommand(s =>
                        {
                            VariantModel newVar = new VariantModel()
                            {
                                Id = Id,
                                ProductId = SelectedProduct.Id,
                                Desc = Description,
                                Price = Price,
                                Quantity = Quantity
                            };

                            if (_mode == Mode.Add)
                            {
                                int result = DataRepository.AddVariant(newVar);
                            }
                            else if (_mode == Mode.Edit)
                            {
                                bool result = DataRepository.EditVariant(newVar);
                            }
                        });
                    }
                }
                catch (Exception e)
                {
                    _logger.Error(e);
                }

                return _save;
            }
        }
        public ICommand Cancel
        {
            get
            {
                try
                {
                    if (_cancel == null)
                    {
                        _cancel = new RelayCommand(s =>
                        {
                            ViewsManager.CloseWindowDialog(this);
                        });
                    }
                }
                catch (Exception e)
                {
                    _logger.Error(e);
                }

                return _cancel;
            }
        }
        #endregion
    }
}
