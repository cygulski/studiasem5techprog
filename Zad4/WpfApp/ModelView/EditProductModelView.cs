﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.ComponentModel;
using System.Collections.ObjectModel;

using NLog;

using WpfApp.Helpers;
using WpfApp.Services;
using DBService.Model;
using DBService;

namespace WpfApp.ModelView
{
    /// <summary>
    /// ModelView for EditProductView.xaml
    /// </summary>
    public class EditProductModelView : INotifyPropertyChanged
    {
        private Logger _logger = NLog.LogManager.GetCurrentClassLogger();

        #region -- Variables -------------------------------
        private ICommand _save = null;
        private ICommand _cancel = null;
        
        private Mode _mode = Mode.Add;

        private string _title = "New product";
        private string _buttonText = "Add";

        private int _id = -1;
        private string _name = "";
        private string _manufacture = "";
        private string _description = "";
        #endregion

        #region -- Events ----------------------------------
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

        #region -- Properties ------------------------------
        public string Title
        {
            get { return _title; }
            set
            {
                _title = value;
                NotifyPropertyChanged("Title");
            }
        }
        
        public string ButtonText
        {
            get { return _buttonText; }
            set
            {
                _buttonText = value;
                NotifyPropertyChanged("ButtonText");
            }
        }

        public int Id
        {
            get { return _id; }
            set
            {
                _id = value;
                NotifyPropertyChanged("Id");
            }
        }
        public string Name
        {
            get { return _name; }
            set
            {
                _name = value;
                NotifyPropertyChanged("Name");
            }
        }
        public string Manufacture
        {
            get { return _manufacture; }
            set
            {
                _manufacture = value;
                NotifyPropertyChanged("Manufacture");
            }
        }
        public string Description
        {
            get { return _description; }
            set
            {
                _description = value;
                NotifyPropertyChanged("Description");
            }
        }
        #endregion

        #region -- Constructors ----------------------------
        public EditProductModelView()
        {

        }

        public EditProductModelView(Mode mode, int id) : this()
        {
            _mode = mode;
            if (mode == Mode.Edit)
            {
                Title = "Edit product - " + id;
                ButtonText = "Save";
                ReloadProduct(id);
            }
        }
        #endregion

        #region -- Methods ---------------------------------
        private void NotifyPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        private void ReloadProduct(int id)
        {
            if (id >= 0)
            {
                ProductModel product = DataRepository.ProductById(id);

                if (product != null)
                {
                    Id = product.Id;
                    Name = product.Name;
                    Manufacture = product.Manufacture;
                    Description = product.Desc;
                }
            }
        }
        #endregion

        #region -- ICommand --------------------------------
        public ICommand Save
        {
            get
            {
                try
                {
                    if (_save == null)
                    {
                        _save = new RelayCommand(s =>
                        {
                            ProductModel newProd = new ProductModel()
                            {
                                Id = Id,
                                Desc = Description,
                                Manufacture = Manufacture,
                                Name = Name
                            };

                            if (_mode == Mode.Add)
                            {
                                int result = DataRepository.AddProduct(newProd);
                            }
                            else if (_mode == Mode.Edit)
                            {
                                bool result = DataRepository.EditProduct(newProd);
                            }
                        });
                    }
                }
                catch (Exception e)
                {
                    _logger.Error(e);
                }

                return _save;
            }
        }
        public ICommand Cancel
        {
            get
            {
                try
                {
                    if (_cancel == null)
                    {
                        _cancel = new RelayCommand(s =>
                        {
                            ViewsManager.CloseWindowDialog(this);
                        });
                    }
                }
                catch (Exception e)
                {
                    _logger.Error(e);
                }

                return _cancel;
            }
        }
        #endregion
    }
}
