﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.ComponentModel;
using System.Collections.ObjectModel;

using NLog;

using WpfApp.Services;
using DBService;
using DBService.Model;
using WpfApp.Helpers;
using System.Windows.Threading;
using System.Threading;

namespace WpfApp.ModelView
{
    /// <summary>
    /// ModelView for ProductsView.xaml
    /// </summary>
    public class ProductsModelView : INotifyPropertyChanged
    {
        private Logger _logger = NLog.LogManager.GetCurrentClassLogger();

        #region -- Variables -------------------------------
        private ICommand _remove = null;
        private ICommand _add = null;
        private ICommand _edit = null;

        private ObservableCollection<ProductModel> _productsList = new ObservableCollection<ProductModel>();
        private ProductModel _selectedProduct = null;
        #endregion

        #region -- Events ----------------------------------
        public event PropertyChangedEventHandler PropertyChanged;
        public event EventHandler DataLoaded;
        #endregion

        #region -- Properties ------------------------------
        public ObservableCollection<ProductModel> ProductsList
        {
            get
            {
                return _productsList;
            }
            set
            {
                _productsList = value;
                NotifyPropertyChanged("ProductsList");
            }
        }
        public ProductModel SelectedProduct
        {
            get
            {
                return _selectedProduct;
            }
            set
            {
                _selectedProduct = value;
                NotifyPropertyChanged("SelectedProduct");
            }
        }
        #endregion

        #region -- Constructors ----------------------------
        public ProductsModelView()
        {
            ReloadProducts();
        }
        #endregion

        #region -- Methods ---------------------------------
        private void NotifyPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        public void ReloadProducts()
        {
            ProductsList.Clear();
            SelectedProduct = null;

            /*IEnumerable<ProductModel> products = DataRepository.AllProducts();
            foreach (ProductModel pr in products)
            {
                ProductsList.Add(pr);
            }*/

            var dispatcher = Dispatcher.CurrentDispatcher;
            Task<IEnumerable<ProductModel>> task = Task<IEnumerable<ProductModel>>.Factory.StartNew(() =>
            {
                return DataRepository.AllProducts();
            });
            Task UITask = task.ContinueWith(x =>
            {
                dispatcher.Invoke(new Action(() =>
                {
                    foreach (ProductModel or in x.Result)
                    {
                        ProductsList.Add(or);
                    }

                    DataLoaded?.Invoke(this, EventArgs.Empty);
                }));
            });
        }
        #endregion

        #region -- ICommand --------------------------------
        public ICommand Remove
        {
            get
            {
                try
                {
                    if (_remove == null)
                    {
                        _remove = new RelayCommand(s =>
                        {
                            if (DataRepository.RemoveProduct(SelectedProduct))
                            {
                                ReloadProducts();
                            }
                        });
                    }
                }
                catch (Exception e)
                {
                    _logger.Error(e);
                }

                return _remove;
            }
        }
        public ICommand Add
        {
            get
            {
                try
                {
                    if (_add == null)
                    {
                        _add = new RelayCommand(s =>
                        {
                            EditProductModelView mv = new EditProductModelView(Mode.Add, -1);
                            ViewsManager.ShowWindowDialog(mv);

                            ReloadProducts();
                        });
                    }
                }
                catch (Exception e)
                {
                    _logger.Error(e);
                }

                return _add;
            }
        }
        public ICommand Edit
        {
            get
            {
                try
                {
                    if (_edit == null)
                    {
                        _edit = new RelayCommand(s =>
                        {
                            EditProductModelView mv = new EditProductModelView(Mode.Edit, SelectedProduct.Id);
                            ViewsManager.ShowWindowDialog(mv);

                            ReloadProducts();
                        });
                    }
                }
                catch (Exception e)
                {
                    _logger.Error(e);
                }

                return _edit;
            }
        }
        #endregion
    }
}
