﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace WpfApp.Services
{
    public class RelayCommand : ICommand
    {
        #region -- Variables -------------------------------
        public event EventHandler CanExecuteChanged;

        private Predicate<object> _canExecute;
        private Action<object> _execute;
        #endregion

        #region -- Constructors ----------------------------
        public RelayCommand(Action<object> execute) : this(execute, null)
        {
            //EMPTY
        }

        public RelayCommand(Action<object> execute, Predicate<object> canExecute)
        {
            this._canExecute = canExecute;
            this._execute = execute;
        }
        #endregion

        #region -- Methods ---------------------------------
        public bool CanExecute(object parameter)
        {
            return _canExecute == null ? true : _canExecute(parameter);
        }

        public void Execute(object parameter)
        {
            _execute(parameter);
        }

        public void RaiseCanExecuteChanged()
        {
            CanExecuteChanged?.Invoke(this, EventArgs.Empty);
        }
        #endregion
    }
}
