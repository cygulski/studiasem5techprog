﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

using WpfApp.Helpers;
using WpfApp.ModelView;
using WpfApp.View;

namespace WpfApp
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private void Application_Startup(object sender, StartupEventArgs e)
        {
            ViewsManager.AddConnection(typeof(EditOrderModelView), typeof(EditOrderView));
            ViewsManager.AddConnection(typeof(EditProductModelView), typeof(EditProductView));
            ViewsManager.AddConnection(typeof(EditUserModelView), typeof(EditUserView));
            ViewsManager.AddConnection(typeof(EditVariantModelView), typeof(EditVariantView));

            ViewsManager.AddConnection(typeof(UsersModelView), typeof(UsersView));
            ViewsManager.AddConnection(typeof(ProductsModelView), typeof(ProductsView));
            ViewsManager.AddConnection(typeof(VariantsModelView), typeof(VariantsView));
        }
    }
}
