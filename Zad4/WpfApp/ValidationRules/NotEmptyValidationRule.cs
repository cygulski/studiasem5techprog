﻿using DBService.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace WpfApp.ValidationRules
{
    public class NotEmptyValidationRule : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            if (value is ObservableCollection<ItemOrderWithProductModel>)
            {
                if ((value as ObservableCollection<ItemOrderWithProductModel>).Count > 0)
                {
                    return ValidationResult.ValidResult;
                }
                return new ValidationResult(false, "Please select item from list");
            }
            return new ValidationResult(false, "Please select item from list");
        }
    }
}
