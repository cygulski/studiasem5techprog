﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace WpfApp.ValidationRules
{
    public class DoubleValidationRule : ValidationRule
    {
        public int MinValue { get; set; }
        public int MaxValue { get; set; }
        public bool UseMaxValue { get; set; }

        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            double val = 0.0;

            try
            {
                if (((string)value).Length > 0)
                    val = Double.Parse((String)value);
            }
            catch (Exception e)
            {
                return new ValidationResult(false, "Illegal characters or " + e.Message);
            }

            if ((val < MinValue) || (UseMaxValue && val > MaxValue))
            {
                return new ValidationResult(false, "Please enter an value in the range: " + MinValue + " - " + MaxValue + ".");
            }
            else
            {
                return ValidationResult.ValidResult;
            }
        }
    }
}
