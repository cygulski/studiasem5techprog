﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace WpfApp.ValidationRules
{
    public class StringValidationRule : ValidationRule
    {
        public int MinLenght { get; set; }
        public int MaxLenght { get; set; }

        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            try
            {
                string val = ((string)value);

                if ((val.Length < MinLenght) || (val.Length > MaxLenght))
                {
                    return new ValidationResult(false,
                      "Lenght of string must be in the range: " + MinLenght + " - " + MaxLenght + ".");
                }
                else
                {
                    return ValidationResult.ValidResult;
                }

            }
            catch (Exception e)
            {
                return new ValidationResult(false, "Not a string or " + e.Message);
            }
        }
    }
}
