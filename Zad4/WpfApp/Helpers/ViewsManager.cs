﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace WpfApp.Helpers
{
    public static class ViewsManager
    {
        private static Dictionary<object, Window> _windows = new Dictionary<object, Window>();
        private static Dictionary<Type, Type> _connections = new Dictionary<Type, Type>();

        public static void AddConnection(Type modelViewType, Type viewType)
        {
            _connections.Add(modelViewType, viewType);
        }
        public static void ShowWindowDialog(object modelView)
        {
            Type modelViewType = modelView.GetType();

            if (!_windows.ContainsKey(modelView) && _connections.ContainsKey(modelViewType))
            {
                Type viewType = _connections[modelViewType];
                Window view = (Window)Activator.CreateInstance(viewType);

                _windows.Add(modelView, view);

                view.DataContext = modelView;
                view.Closed += (s, e) =>
                {
                    _windows.Remove(modelView);
                };
                view.ShowDialog();
            }
        }
        public static void CloseWindowDialog(object modelView)
        {
            if (_windows.ContainsKey(modelView))
            {
                Window view = _windows[modelView];
                view.Close();
            }
        }
    }
}
