﻿using System;
using System.Collections;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

using DBService;
using DBService.Model;

namespace Tests
{
    [TestClass]
    public class DataRepositoryTests
    {
        #region Orders
        [TestMethod]
        public void AllOrders()
        {
            List<OrderModel> data = DataRepository.AllOrders().ToList();
            Assert.AreNotEqual(data, null);
            Assert.AreEqual(data.Count > 0, true);
        }
        [TestMethod]
        public void AllOrdersWtihUsers()
        {
            List<OrderWithUserModel> data = DataRepository.AllOrdersWtihUsers().ToList();
            Assert.AreNotEqual(data, null);
            Assert.AreEqual(data.Count > 0, true);
        }
        [TestMethod]
        public void OrderById()
        {
            OrderModel order = DataRepository.AllOrders().FirstOrDefault();
            Assert.AreNotEqual(order, null);

            OrderModel data = DataRepository.OrderById(order.Id);
            Assert.AreNotEqual(data, null);

            Assert.AreEqual(data.Id, order.Id);
            Assert.AreEqual(data.Date, order.Date);
            Assert.AreEqual(data.UserId, order.UserId);
        }
        [TestMethod]
        public void AddOrder()
        {
            OrderModel order = new OrderModel()
            {
                Id = -1,
                Date = DateTime.Now,
                UserId = 0
            };

            int id = DataRepository.AddOrder(order);
            Assert.AreEqual(id >= 0, true);

            OrderModel data = DataRepository.OrderById(id);
            Assert.AreNotEqual(data, null);

            Assert.AreEqual(data.Id, id);
            Assert.AreEqual(data.Date.ToShortDateString(), order.Date.ToShortDateString());
            Assert.AreEqual(data.UserId, order.UserId);

            DataRepository.RemoveOrder(data);
        }
        [TestMethod]
        public void EditOrder()
        {
            OrderModel order = DataRepository.AllOrders().FirstOrDefault();
            Assert.AreNotEqual(order, null);

            bool result = DataRepository.EditOrder(order);
            Assert.AreEqual(result, true);

            OrderModel data = DataRepository.OrderById(order.Id);
            Assert.AreNotEqual(data, null);

            Assert.AreEqual(data.Id, order.Id);
            Assert.AreEqual(data.Date.ToShortDateString(), order.Date.ToShortDateString());
            Assert.AreEqual(data.UserId, order.UserId);
        }
        [TestMethod]
        public void RemoveOrder()
        {
            OrderModel order = new OrderModel()
            {
                Id = -1,
                Date = DateTime.Now,
                UserId = 0
            };

            int id = DataRepository.AddOrder(order);
            Assert.AreEqual(id >= 0, true);

            OrderModel data = DataRepository.OrderById(id);
            Assert.AreNotEqual(data, null);

            DataRepository.RemoveOrder(data);

            OrderModel data2 = DataRepository.OrderById(id);
            Assert.AreEqual(data2, null);
        }
        #endregion

        #region OrderItems
        [TestMethod]
        public void AllOrderItemsWithProduct()
        {
            OrderModel order = DataRepository.AllOrders().FirstOrDefault();
            Assert.AreNotEqual(order, null);

            List<ItemOrderWithProductModel> data = DataRepository.AllOrderItemsWithProduct(order.Id).ToList();
            Assert.AreNotEqual(data, null);
            Assert.AreEqual(data.Count > 0, true);
        }
        [TestMethod]
        public void AddOrderItem()
        {
            ItemOrderWithProductModel item = new ItemOrderWithProductModel()
            {
                Id = -1,
                OrderId = 0,
                ProductId = 0,
                VariantId = 0,
                Quantity = 4,
            };

            int id = DataRepository.AddOrderItem(item, item.OrderId);
            Assert.AreEqual(id >= 0, true);

            ItemOrderWithProductModel data = DataRepository.OrderItemById(id);
            Assert.AreNotEqual(data, null);

            Assert.AreEqual(data.Id, id);
            Assert.AreEqual(data.OrderId, item.OrderId);
            Assert.AreEqual(data.VariantId, item.VariantId);
            Assert.AreEqual(data.Quantity, item.Quantity);

            DataRepository.RemoveOrderItem(data);
        }
        [TestMethod]
        public void EditOrderItem()
        {
            OrderModel order = DataRepository.AllOrders().FirstOrDefault();
            Assert.AreNotEqual(order, null);

            ItemOrderWithProductModel orderItem = DataRepository.AllOrderItemsWithProduct(order.Id).FirstOrDefault();
            Assert.AreNotEqual(orderItem, null);

            bool result = DataRepository.EditOrderItem(orderItem);
            Assert.AreEqual(result, true);

            ItemOrderWithProductModel data = DataRepository.OrderItemById(orderItem.Id);
            Assert.AreNotEqual(data, null);

            Assert.AreEqual(data.Id, orderItem.Id);
            Assert.AreEqual(data.OrderId, orderItem.OrderId);
            Assert.AreEqual(data.VariantId, orderItem.ProductId);
            Assert.AreEqual(data.Quantity, orderItem.Quantity);
        }
        [TestMethod]
        public void RemoveOrderItem()
        {
            ItemOrderWithProductModel item = new ItemOrderWithProductModel()
            {
                Id = -1,
                OrderId = 0,
                ProductId = 0,
                VariantId = 0,
                Quantity = 4,
            };

            int id = DataRepository.AddOrderItem(item, item.OrderId);
            Assert.AreEqual(id >= 0, true);

            ItemOrderWithProductModel data = DataRepository.OrderItemById(id);
            Assert.AreNotEqual(data, null);

            DataRepository.RemoveOrderItem(data);

            ItemOrderWithProductModel data2 = DataRepository.OrderItemById(id);
            Assert.AreEqual(data2, null);
        }
        #endregion

        #region Users
        [TestMethod]
        public void AllUsers()
        {
            List<UserModel> data = DataRepository.AllUsers().ToList();
            Assert.AreNotEqual(data, null);
            Assert.AreEqual(data.Count > 0, true);
        }
        [TestMethod]
        public void UserById()
        {
            UserModel user = DataRepository.AllUsers().FirstOrDefault();
            Assert.AreNotEqual(user, null);

            UserModel data = DataRepository.UserById(user.Id);
            Assert.AreNotEqual(data, null);

            Assert.AreEqual(data.Id, user.Id);
            Assert.AreEqual(data.Login, user.Login);
            Assert.AreEqual(data.Name, user.Name);
            Assert.AreEqual(data.Password, user.Password);
            Assert.AreEqual(data.Surname, user.Surname);
        }
        [TestMethod]
        public void AddUser()
        {
            UserModel user = new UserModel()
            {
                Id = -1,
                Login = "1",
                Name = "2",
                Password = "3",
                Surname = "4"
            };

            int id = DataRepository.AddUser(user);
            Assert.AreEqual(id >= 0, true);

            UserModel data = DataRepository.UserById(id);
            Assert.AreNotEqual(data, null);

            Assert.AreEqual(data.Id, id);
            Assert.AreEqual(data.Login, user.Login);
            Assert.AreEqual(data.Name, user.Name);
            Assert.AreEqual(data.Password, user.Password);
            Assert.AreEqual(data.Surname, user.Surname);

            DataRepository.RemoveUser(data);
        }
        [TestMethod]
        public void EditUser()
        {
            UserModel user = DataRepository.AllUsers().FirstOrDefault();
            Assert.AreNotEqual(user, null);

            bool result = DataRepository.EditUser(user);
            Assert.AreEqual(result, true);

            UserModel data = DataRepository.UserById(user.Id);
            Assert.AreNotEqual(data, null);

            Assert.AreEqual(data.Id, user.Id);
            Assert.AreEqual(data.Login, user.Login);
            Assert.AreEqual(data.Name, user.Name);
            Assert.AreEqual(data.Password, user.Password);
            Assert.AreEqual(data.Surname, user.Surname);
        }
        [TestMethod]
        public void RemoveUser()
        {
            UserModel user = new UserModel()
            {
                Id = -1,
                Login = "1",
                Name = "2",
                Password = "3",
                Surname = "4"
            };

            int id = DataRepository.AddUser(user);
            Assert.AreEqual(id >= 0, true);

            UserModel data = DataRepository.UserById(id);
            Assert.AreNotEqual(data, null);

            DataRepository.RemoveUser(data);

            UserModel data2 = DataRepository.UserById(id);
            Assert.AreEqual(data2, null);
        }
        #endregion

        #region Products
        [TestMethod]
        public void AllProducts()
        {
            List<ProductModel> data = DataRepository.AllProducts().ToList();
            Assert.AreNotEqual(data, null);
            Assert.AreEqual(data.Count > 0, true);
        }
        [TestMethod]
        public void ProductById()
        {
            ProductModel product = DataRepository.AllProducts().FirstOrDefault();
            Assert.AreNotEqual(product, null);

            ProductModel data = DataRepository.ProductById(product.Id);
            Assert.AreNotEqual(data, null);

            Assert.AreEqual(data.Id, product.Id);
            Assert.AreEqual(data.Manufacture, product.Manufacture);
            Assert.AreEqual(data.Name, product.Name);
            Assert.AreEqual(data.Desc, product.Desc);
        }
        [TestMethod]
        public void AddProduct()
        {
            ProductModel product = new ProductModel()
            {
                Id = -1,
                Name = "2",
                Desc = "3",
                Manufacture = "4"
            };

            int id = DataRepository.AddProduct(product);
            Assert.AreEqual(id >= 0, true);

            ProductModel data = DataRepository.ProductById(id);
            Assert.AreNotEqual(data, null);

            Assert.AreEqual(data.Id, id);
            Assert.AreEqual(data.Name, product.Name);
            Assert.AreEqual(data.Desc, product.Desc);
            Assert.AreEqual(data.Manufacture, product.Manufacture);

            DataRepository.RemoveProduct(data);
        }
        [TestMethod]
        public void EditProduct()
        {
            ProductModel product = DataRepository.AllProducts().FirstOrDefault();
            Assert.AreNotEqual(product, null);

            bool result = DataRepository.EditProduct(product);
            Assert.AreEqual(result, true);

            ProductModel data = DataRepository.ProductById(product.Id);
            Assert.AreNotEqual(data, null);

            Assert.AreEqual(data.Id, product.Id);
            Assert.AreEqual(data.Name, product.Name);
            Assert.AreEqual(data.Desc, product.Desc);
            Assert.AreEqual(data.Manufacture, product.Manufacture);
        }
        [TestMethod]
        public void RemoveProduct()
        {
            ProductModel product = new ProductModel()
            {
                Id = -1,
                Name = "2",
                Desc = "3",
                Manufacture = "4"
            };

            int id = DataRepository.AddProduct(product);
            Assert.AreEqual(id >= 0, true);

            ProductModel data = DataRepository.ProductById(id);
            Assert.AreNotEqual(data, null);

            DataRepository.RemoveProduct(data);

            ProductModel data2 = DataRepository.ProductById(id);
            Assert.AreEqual(data2, null);
        }
        #endregion

        #region Variants
        [TestMethod]
        public void AllVariants()
        {
            List<VariantModel> data = DataRepository.AllVariants().ToList();
            Assert.AreNotEqual(data, null);
            Assert.AreEqual(data.Count > 0, true);
        }
        [TestMethod]
        public void AllVariantsWithProducts()
        {
            List<VariantWithProductModel> data = DataRepository.AllVariantsWithProducts().ToList();
            Assert.AreNotEqual(data, null);
            Assert.AreEqual(data.Count > 0, true);
        }
        [TestMethod]
        public void VariantById()
        {
            VariantModel variant = DataRepository.AllVariants().FirstOrDefault();
            Assert.AreNotEqual(variant, null);

            VariantModel data = DataRepository.VariantById(variant.Id);
            Assert.AreNotEqual(data, null);

            Assert.AreEqual(data.Id, variant.Id);
            Assert.AreEqual(data.Price, variant.Price);
            Assert.AreEqual(data.ProductId, variant.ProductId);
            Assert.AreEqual(data.Desc, variant.Desc);
            Assert.AreEqual(data.Quantity, variant.Quantity);
        }
        [TestMethod]
        public void AddVariant()
        {
            VariantModel variant = new VariantModel()
            {
                Id = -1,
                ProductId = 0,
                Desc = "3",
                Price = 4,
                Quantity = 5
            };

            int id = DataRepository.AddVariant(variant);
            Assert.AreEqual(id >= 0, true);

            VariantModel data = DataRepository.VariantById(id);
            Assert.AreNotEqual(data, null);

            Assert.AreEqual(data.Id, id);
            Assert.AreEqual(data.ProductId, variant.ProductId);
            Assert.AreEqual(data.Desc, variant.Desc);
            Assert.AreEqual(data.Price, variant.Price);
            Assert.AreEqual(data.Quantity, variant.Quantity);

            DataRepository.RemoveVariant(data);
        }
        [TestMethod]
        public void EditVariant()
        {
            VariantModel variant = DataRepository.AllVariants().FirstOrDefault();
            Assert.AreNotEqual(variant, null);

            bool result = DataRepository.EditVariant(variant);
            Assert.AreEqual(result, true);

            VariantModel data = DataRepository.VariantById(variant.Id);
            Assert.AreNotEqual(data, null);

            Assert.AreEqual(data.Id, variant.Id);
            Assert.AreEqual(data.ProductId, variant.ProductId);
            Assert.AreEqual(data.Desc, variant.Desc);
            Assert.AreEqual(data.Price, variant.Price);
            Assert.AreEqual(data.Quantity, variant.Quantity);
        }
        [TestMethod]
        public void RemoveVariant()
        {
            VariantModel variant = new VariantModel()
            {
                Id = -1,
                ProductId = 0,
                Desc = "3",
                Price = 4,
                Quantity = 5
            };

            int id = DataRepository.AddVariant(variant);
            Assert.AreEqual(id >= 0, true);

            VariantModel data = DataRepository.VariantById(id);
            Assert.AreNotEqual(data, null);

            DataRepository.RemoveVariant(data);

            VariantModel data2 = DataRepository.VariantById(id);
            Assert.AreEqual(data2, null);
        }
        #endregion
    }
}
