﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using WpfApp.ModelView;
using DBService.Model;
using DBService;
using System.Threading;

namespace Tests
{
    [TestClass]
    public class ModelViewsTests
    {
        #region UsersModelView
        [TestMethod]
        public void UsersModelView_Refresh()
        {
            UsersModelView mv = new UsersModelView();
            mv.DataLoaded += (s, e) =>
            {
                Assert.AreEqual(mv.UsersList.Count > 0, true);
            };
        }
        #endregion

        #region EditUsersModelView
        [TestMethod]
        public void EditUsersModelView_Edit_Refresh()
        {
            UserModel user = DataRepository.AllUsers().FirstOrDefault();

            EditUserModelView mv = new EditUserModelView(WpfApp.Helpers.Mode.Edit, user.Id);
            Assert.AreEqual(mv.Id, user.Id);
            Assert.AreEqual(mv.Login, user.Login);
            Assert.AreEqual(mv.Name, user.Name);
            Assert.AreEqual(mv.Password, user.Password);
            Assert.AreEqual(mv.Surname, user.Surname);
        }
        #endregion

        #region ProductsModelView
        [TestMethod]
        public void ProductsModelView_Refresh()
        {
            ProductsModelView mv = new ProductsModelView();
            mv.DataLoaded += (s, e) =>
            {
                Assert.AreEqual(mv.ProductsList.Count > 0, true);
            };
        }
        #endregion

        #region EditProductsModelView
        [TestMethod]
        public void EditProductsModelView_Edit_Refresh()
        {
            ProductModel product = DataRepository.AllProducts().FirstOrDefault();

            EditProductModelView mv = new EditProductModelView(WpfApp.Helpers.Mode.Edit, product.Id);
            Assert.AreEqual(mv.Id, product.Id);
            Assert.AreEqual(mv.Name, product.Name);
            Assert.AreEqual(mv.Manufacture, product.Manufacture);
            Assert.AreEqual(mv.Description, product.Desc);
        }
        #endregion

        #region OrdersModelView
        [TestMethod]
        public void OrdersModelView_Refresh()
        {
            OrdersModelView mv = new OrdersModelView();
            mv.DataLoaded += (s, e) =>
            {
                Assert.AreEqual(mv.OrdersList.Count > 0, true);
            };
        }
        #endregion

        #region EditOrdersModelView
        [TestMethod]
        public void EditOrdersModelView_Edit_Refresh()
        {
            OrderModel order = DataRepository.AllOrders().FirstOrDefault();

            EditOrderModelView mv = new EditOrderModelView(WpfApp.Helpers.Mode.Edit, order.Id);
            Assert.AreEqual(mv.Id, order.Id);
            Assert.AreEqual(mv.Date.ToShortDateString(), order.Date.ToShortDateString());
            Assert.AreEqual(mv.SelectedUser.Id, order.UserId);

            Assert.AreEqual(mv.Users.Count > 0, true);
            Assert.AreEqual(mv.Variants.Count > 0, true);
            Assert.AreEqual(mv.Items.Count > 0, true);
        }
        [TestMethod]
        public void EditOrdersModelView_Add_Refresh()
        {
            EditOrderModelView mv = new EditOrderModelView(WpfApp.Helpers.Mode.Add, -1);
            Assert.AreEqual(mv.Users.Count > 0, true);
            Assert.AreEqual(mv.Variants.Count > 0, true);
        }
        #endregion

        #region VariantsModelView
        [TestMethod]
        public void VariantsModelView_Refresh()
        {
            VariantsModelView mv = new VariantsModelView();
            mv.DataLoaded += (s, e) =>
            {
                Assert.AreEqual(mv.VariantsList.Count > 0, true);
            };
        }
        #endregion

        #region EditVariantsModelView
        [TestMethod]
        public void EditVariantsModelView_Edit_Refresh()
        {
            VariantModel order = DataRepository.AllVariants().FirstOrDefault();

            EditVariantModelView mv = new EditVariantModelView(WpfApp.Helpers.Mode.Edit, order.Id);
            Assert.AreEqual(mv.Id, order.Id);
            Assert.AreEqual(mv.Description, order.Desc);
            Assert.AreEqual(mv.Price, order.Price);
            Assert.AreEqual(mv.Quantity, order.Quantity);
            Assert.AreEqual(mv.SelectedProduct.Id, order.ProductId);

            Assert.AreEqual(mv.ProductsList.Count > 0, true);
        }
        [TestMethod]
        public void EditVariantsModelView_Add_Refresh()
        {
            EditVariantModelView mv = new EditVariantModelView(WpfApp.Helpers.Mode.Add, -1);
            Assert.AreEqual(mv.ProductsList.Count > 0, true);
        }
        #endregion
    }
}
