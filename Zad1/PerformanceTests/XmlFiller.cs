﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using Library;

namespace PerformanceTests
{
    public class XmlFiller : IFiller
    {
        public class FileDataContext
        {
            public List<Client> Clients = new List<Client>();
            public List<Product> Products = new List<Product>();
            public List<ProductVariant> ProductVariants = new List<ProductVariant>();
            public List<AEvent> Events = new List<AEvent>();
        }

        protected string filePath = "baza.xml";

        public XmlFiller()
        {

        }
        public XmlFiller(string file)
        {
            filePath = file;
        }

        public void FillData(DataContext dataContext)
        {
            if (!File.Exists(filePath))
                throw new FileNotFoundException();

            FileDataContext file = Deserialize(filePath);
            FileDataContextToDataContext(file, dataContext);
        }
        public void SaveData(DataContext dataContext)
        {
            FileDataContext file = DataContectToFileDataContext(dataContext);
            Serialize(filePath, file);
        }

        protected FileDataContext DataContectToFileDataContext(DataContext context)
        {
            #region RepairRelativeIds
            foreach (ProductVariant pv in context.ProductsVariants)
                pv.ProductId = pv.Product.Id;
            foreach (AEvent or in context.Events)
            {
                or.ProductVariantId = or.ProductVariant.Id;
                or.ClientId = or.Client.Id;
            }
            #endregion

            FileDataContext file = new FileDataContext();

            file.Clients.AddRange(context.Clients);
            file.Products.AddRange(context.Products.Values);
            file.ProductVariants.AddRange(context.ProductsVariants);
            file.Events.AddRange(context.Events);

            return file;

        }
        protected void FileDataContextToDataContext(FileDataContext file, DataContext context)
        {
            #region RepairRelativeObjects
            foreach (ProductVariant vr in file.ProductVariants)
            {
                Product pr = file.Products.FirstOrDefault(x => x.Id == vr.ProductId);

                vr.Product = pr;
            }
            foreach (AEvent or in file.Events)
            {
                Client cl = file.Clients.FirstOrDefault(x => x.Id == or.ClientId);
                ProductVariant pv = file.ProductVariants.FirstOrDefault(x => x.Id == or.ProductVariantId);

                or.Client = cl;
                or.ProductVariant = pv;
            }
            #endregion

            context.Clients.AddRange(file.Clients);
            foreach (Product pr in file.Products)
                context.Products.Add(pr.Id, pr);
            foreach (ProductVariant pv in file.ProductVariants)
                context.ProductsVariants.Add(pv);
            foreach (AEvent or in file.Events)
                context.Events.Add(or);
        }

        protected FileDataContext Deserialize(string file)
        {
            Type[] types = new Type[1] { typeof(Order) };
            XmlSerializer ser = new XmlSerializer(typeof(FileDataContext), types);
            using (XmlReader reader = XmlReader.Create(file))
                return (FileDataContext) ser.Deserialize(reader);
        }
        protected void Serialize(string file, FileDataContext data)
        {
            Type[] types = new Type[1] { typeof(Order) };
            XmlSerializer ser = new XmlSerializer(typeof(FileDataContext), types);

            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
            settings.IndentChars = "    ";

            using (XmlWriter writer =  XmlWriter.Create(file, settings))
                ser.Serialize(writer, data);
        }
    }
}
