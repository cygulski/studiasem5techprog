﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Library;

namespace PerformanceTests
{
    public class RandomFiller : IFiller
    {
        private int dataCount = 25;

        private List<string> names = new List<string>() { "Ania", "Patryk", "Michał", "Paweł", "Kamil", "Magda", "Monika", "Agnieszka" };
        private List<string> surnames = new List<string>() { "Krawczyk", "Mak", "Baran", "Kawka", "Struś", "Wróbel", "Szpak", "Królik" };
        private List<string> citys = new List<string>() { "Łódź", "Warszawa", "Poznań", "Kraków", "Gdańsk" };
        private List<string> streets = new List<string>() { "Mariacka", "Cieszyńska", "Piłsudskiego", "1 Maja", "Krasickiego", "Sienkiewicza", "Mickiewicza", "Tuwima", "Żeromskiego" };

        private List<string> manufactures = new List<string>() { "Microsoft", "Adobe", "Autodesk", "Ubisoft", "Ea", "Blizzard" };

        public int DataCount
        {
            get { return dataCount * 4; }
            set
            {
                if (value < 4)
                    dataCount = 1;
                else
                    dataCount = (int)(value  / 4.0);
            }
        }
        public bool SerializeData { get; set; } = false;

        private string StringGenerator(int lenghtMin, int lengthMax, Random rand)
        {
            int lenght = rand.Next(lenghtMin, lengthMax);
            string letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            char[] word = new char[lenght];

            for(int i = 0; i < word.Length; i++)
                word[i] = letters[rand.Next(letters.Length - 1)];

            return new string(word);
        }

        public void FillData(DataContext dataContext)
        {
            Random rand = new Random();

            for (int i = 0; i < dataCount; i++)
            {
                #region Clients
                string name = names[rand.Next(names.Count)];
                string surname = surnames[rand.Next(surnames.Count)];
                string city = citys[rand.Next(citys.Count)];
                string street = streets[rand.Next(streets.Count)];
                int code1 = rand.Next(10, 99);
                int code2 = rand.Next(100, 999);
                int number = rand.Next(1, 99);

                Client cl = new Client(i, name, surname, 
                    string.Format("{0}_{1}@gmail.com", name, surname), 
                    string.Format("{0}-{1} {2}, {3} {4}", code1, code2, city, street, number));
                dataContext.Clients.Add(cl);
                #endregion


                #region Products
                string manufacture = manufactures[rand.Next(manufactures.Count)];
                string productName = StringGenerator(6, 12, rand);
                string description = StringGenerator(9, 24, rand);

                Product pr = new Product(i, productName, manufacture, description);
                dataContext.Products.Add(i, pr);
                #endregion


                #region ProductVariants
                Product randomProduct = dataContext.Products[rand.Next(dataContext.Products.Count)];
                int quantity = rand.Next(0, 200);
                int price = rand.Next(1, 999);
                string variantDescription = StringGenerator(9, 24, rand);

                ProductVariant pv = new ProductVariant(i, randomProduct, quantity, price, variantDescription);
                dataContext.ProductsVariants.Add(pv);
                #endregion


                #region Order
                ProductVariant randomProductVariant = dataContext.ProductsVariants[rand.Next(dataContext.ProductsVariants.Count)];
                Client randomClient = dataContext.Clients[rand.Next(dataContext.Clients.Count)];
                int orderQuantity = rand.Next(0, 200);
                int year = rand.Next(2001, 2017);
                int month = rand.Next(1, 12);
                int day = rand.Next(1, 28);
                DateTime date = new DateTime(year, month, day);
                
                Order or = new Order(i, randomProductVariant, randomClient, orderQuantity, date);
                dataContext.Events.Add(or);
                #endregion
            }

            #region TestSaveToFile
            if (SerializeData)
            {
                XmlFiller filler = new XmlFiller(string.Format("baza_{0}.xml", DataCount));
                filler.SaveData(dataContext);
            }
            #endregion
        }
    }
}
