﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Library;
using System.IO;

namespace PerformanceTests
{
    [TestClass]
    public class FillersTests
    {
        [TestMethod]
        public void RandomFiller_DataFill_8()
        {
            DataRepository rep = new DataRepository();
            RandomFiller filler = new RandomFiller()
            {
                DataCount = 8
            };
            rep.Filler = filler;
        }
        [TestMethod]
        public void RandomFiller_DataFill_100()
        {
            DataRepository rep = new DataRepository();
            RandomFiller filler = new RandomFiller()
            {
                DataCount = 100
            };
            rep.Filler = filler;
        }
        [TestMethod]
        public void RandomFiller_DataFill_1000()
        {
            DataRepository rep = new DataRepository();
            RandomFiller filler = new RandomFiller()
            {
                DataCount = 1000
            };
            rep.Filler = filler;
        }
        [TestMethod]
        public void RandomFiller_DataFill_10000()
        {
            DataRepository rep = new DataRepository();
            RandomFiller filler = new RandomFiller()
            {
                DataCount = 10000
            };
            rep.Filler = filler;
        }
        [TestMethod]
        public void RandomFiller_DataFill_100000()
        {
            DataRepository rep = new DataRepository();
            RandomFiller filler = new RandomFiller()
            {
                DataCount = 100000
            };
            rep.Filler = filler;
        }
        
        [TestMethod]
        public void RandomFiller_SaveData_8()
        {
            DataRepository rep = new DataRepository();
            RandomFiller filler = new RandomFiller()
            {
                DataCount = 8,
                SerializeData = true
            };
            rep.Filler = filler;
        }
        [TestMethod]
        public void RandomFiller_SaveData_100()
        {
            DataRepository rep = new DataRepository();
            RandomFiller filler = new RandomFiller()
            {
                DataCount = 100,
                SerializeData = true
            };
            rep.Filler = filler;
        }
        [TestMethod]
        public void RandomFiller_SaveData_1000()
        {
            DataRepository rep = new DataRepository();
            RandomFiller filler = new RandomFiller()
            {
                DataCount = 1000,
                SerializeData = true
            };
            rep.Filler = filler;
        }
        [TestMethod]
        public void RandomFiller_SaveData_10000()
        {
            DataRepository rep = new DataRepository();
            RandomFiller filler = new RandomFiller()
            {
                DataCount = 10000,
                SerializeData = true
            };
            rep.Filler = filler;
        }
        [TestMethod]
        public void RandomFiller_SaveData_100000()
        {
            DataRepository rep = new DataRepository();
            RandomFiller filler = new RandomFiller()
            {
                DataCount = 100000,
                SerializeData = true
            };
            rep.Filler = filler;
        }

        [TestMethod]
        public void XmlFiller_DataFill_8()
        {
            DataRepository rep = new DataRepository();
            XmlFiller filler = new XmlFiller("baza_8.xml");
            rep.Filler = filler;
        }
        [TestMethod]
        public void XmlFiller_DataFill_100()
        {
            DataRepository rep = new DataRepository();
            XmlFiller filler = new XmlFiller("baza_100.xml");
            rep.Filler = filler;
        }
        [TestMethod]
        public void XmlFiller_DataFill_1000()
        {
            DataRepository rep = new DataRepository();
            XmlFiller filler = new XmlFiller("baza_1000.xml");
            rep.Filler = filler;
        }
        [TestMethod]
        public void XmlFiller_DataFill_10000()
        {
            DataRepository rep = new DataRepository();
            XmlFiller filler = new XmlFiller("baza_10000.xml");
            rep.Filler = filler;
        }
        [TestMethod]
        [ExpectedException(typeof(FileNotFoundException))]
        public void XmlFiller_DataFill_FileNotFound()
        {
            DataRepository rep = new DataRepository();
            XmlFiller filler = new XmlFiller("notAFile.xml");
            rep.Filler = filler;
        }
        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void XmlFiller_DataFill_WrongFileFormat()
        {
            DataRepository rep = new DataRepository();
            XmlFiller filler = new XmlFiller("Library.dll");
            rep.Filler = filler;
        }
    }
}
