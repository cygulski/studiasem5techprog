﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Library;
using Tests;

namespace Application
{
    class Program
    {
        static void Main(string[] args)
        {
            DataRepository rep = new DataRepository(new StaticFiller());
            DataService ser = new DataService(rep);

            #region Prints
            Console.WriteLine("----- Print All Clients -----\n");
            ser.PrintClients(rep.GetAllClients());

            Console.WriteLine("");
            Console.WriteLine("\n----- Print All Products -----\n");
            ser.PrintProducts(rep.GetAllProducts());

            Console.WriteLine("");
            Console.WriteLine("\n----- Print All ProductVarians -----\n");
            ser.PrintProductVariants(rep.GetAllProductVariants());

            Console.WriteLine("");
            Console.WriteLine("\n----- Print All Orders -----\n");
            ser.PrintEvents(rep.GetAllEvents());
            
            Console.WriteLine("");
            Console.WriteLine("");
            Console.WriteLine("\n----- Print All -----\n");
            ser.PrintAll();
            #endregion

            #region Add
            Console.WriteLine("");
            Console.WriteLine("");
            Console.WriteLine("\n----- Add Client -----\n");
            ser.AddClient("Michał", "Kuszewski", "kuszewski@gmail.com", "34-234 Kuszewice, ul. Bajkowa 4");
            ser.AddClient("Ania", "Malińska", "test@gmail.com", "34-224 Kuszewice, ul. Mleczna 24");

            Console.WriteLine("");
            Console.WriteLine("\n----- Add Product -----\n");
            ser.AddProduct("Rayman 2", "Ubisoft", "Game");
            ser.AddProduct("Rayman 3", "Ubisoft", "Game");
            ser.AddProduct("Rayman 4", "Ubisoft", "Game");

            Console.WriteLine("");
            Console.WriteLine("\n----- Add Product Variant -----\n");
            ser.AddProductVariant(3, 15, 5.0, "PC version");
            ser.AddProductVariant(3, 15, 5.0, "GameCube version");

            Console.WriteLine("");
            Console.WriteLine("\n----- Add Product Variant <Wrong Id> -----\n");
            ser.AddProductVariant(9, 15, 5.0, "PC version");

            Console.WriteLine("");
            Console.WriteLine("\n----- Add Order -----\n");
            ser.AddOrder(2, 3, 1);

            Console.WriteLine("");
            Console.WriteLine("\n----- Add Order <Wrong Client Id> -----\n");
            ser.AddOrder(8, 3, 1);

            Console.WriteLine("");
            Console.WriteLine("\n----- Add Order <Wrong Product Variant Id> -----\n");
            ser.AddOrder(2, 7, 1);
            #endregion

            #region Remove
            Console.WriteLine("");
            Console.WriteLine("");
            Console.WriteLine("\n----- Remove Client -----\n");
            ser.RemoveClient(4);

            Console.WriteLine("");
            Console.WriteLine("\n----- Remove Client <Wrong Id> -----\n");
            ser.RemoveClient(8);

            Console.WriteLine("");
            Console.WriteLine("\n----- Remove Client <Client Has Order History> -----\n");
            ser.RemoveClient(2);

            Console.WriteLine("");
            Console.WriteLine("\n----- Remove Product -----\n");
            ser.RemoveProduct(5);

            Console.WriteLine("");
            Console.WriteLine("\n----- Remove Product <Wrong Id> -----\n");
            ser.RemoveProduct(9);

            Console.WriteLine("");
            Console.WriteLine("\n----- Remove Product <Product Has Variants> -----\n");
            ser.RemoveProduct(3);
            
            Console.WriteLine("");
            Console.WriteLine("\n----- Remove Product Variant -----\n");
            ser.RemoveProductVariant(4);

            Console.WriteLine("");
            Console.WriteLine("\n----- Remove Product Variant <Wrong Id> -----\n");
            ser.RemoveProductVariant(9);

            Console.WriteLine("");
            Console.WriteLine("\n----- Remove Product Variant <Product Variant Used In Orders> -----\n");
            ser.RemoveProductVariant(3);
            
            Console.WriteLine("");
            Console.WriteLine("\n----- Remove Order -----\n");
            ser.RemoveEvent(5);
            
            Console.WriteLine("");
            Console.WriteLine("\n----- Remove Order <Wrong Id> -----\n");
            ser.RemoveEvent(9);
            #endregion

            #region Data filter and search
            Console.WriteLine("");
            Console.WriteLine("");
            Console.WriteLine("\n----- Filter Products By Manufacture -----\n");
            ser.FilterProductsByManufacture("Ubisoft");
            
            Console.WriteLine("");
            Console.WriteLine("\n----- Filter Low Quantity Variants -----\n");
            ser.FilterLowQuantityVariants();

            Console.WriteLine("");
            Console.WriteLine("\n----- Search Orders Between Dates -----\n");
            ser.SearchOrdersBetweenDates(new DateTime(2010,4,21), DateTime.Now);

            Console.WriteLine("");
            Console.WriteLine("\n----- Search Variants Between Price -----\n");
            ser.SearchVariantsBetweenPrice(2.0, 10.0);
            #endregion

            Console.WriteLine("");
            Console.WriteLine("");
            Console.WriteLine("\n----- Press Any Key To Exit -----\n");
            Console.ReadKey();
        }
    }
}
