﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Library;
using System.Collections.ObjectModel;

namespace Application
{
    public class DataService
    {
        protected DataRepository dataRepository;
        
        public DataService(DataRepository dataRep)
        {
            dataRepository = dataRep;

            dataRepository.EventsChanged += DataRepository_EventsChanged;
            dataRepository.ProductsVariantsChanged += DataRepository_ProductsVariantsChanged;
        }

        private void DataRepository_ProductsVariantsChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            Console.WriteLine(" > Event: ProductVariants Changed");
        }
        private void DataRepository_EventsChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            Console.WriteLine(" > Event: Events Changed");
        }


        #region Prints on console
        public void PrintClients(List<Client> clients)
        {
            foreach (Client cl in clients)
                Console.WriteLine(cl.ToString());
        }
        public void PrintProducts(Dictionary<int, Product> products)
        {
            foreach (Product pr in products.Values)
                Console.WriteLine(pr.ToString());
        }
        public void PrintProductVariants(ObservableCollection<ProductVariant> variants)
        {
            foreach (ProductVariant vr in variants)
                Console.WriteLine(vr.ToString());
        }
        public void PrintEvents(ObservableCollection<AEvent> events)
        {
            foreach (AEvent or in events)
                Console.WriteLine(or.ToString());
        }

        public void PrintAll()
        {
            try
            {
                if (dataRepository != null)
                {
                    foreach (Client cl in dataRepository.GetAllClients())
                    {
                        Console.WriteLine("├─Client: {0}", cl.ToString());

                        List<AEvent> events = dataRepository.GetAllEvents().Where(x => x.Client == cl).ToList();
                        for (int i = 0; i < events.Count; i++)
                        {
                            AEvent or = events[i];
                            if (i == events.Count - 1)
                            {
                                Console.WriteLine("│   └─Event:{0}   {1}", or.GetType().Name, or.ToString());
                                Console.WriteLine("│      └─Variant: {0}", or.ProductVariant.ToString());
                                Console.WriteLine("│         └─Product{0}", or.ProductVariant.Product.ToString());
                            }
                            else
                            {
                                Console.WriteLine("│   ├─Event:{0}   {1}", or.GetType().Name, or.ToString());
                                Console.WriteLine("│   │  └─Variant: {0}", or.ProductVariant.ToString());
                                Console.WriteLine("│   │     └─Product: {0}", or.ProductVariant.Product.ToString());
                                Console.WriteLine("│   │");
                            }
                        }

                        Console.WriteLine("│");
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(string.Format("Exception : {0}", e.Message));
            }
        }
        #endregion


        #region Helpers
        private int LastClientId()
        {
            return dataRepository.GetAllClients().Max(x => x.Id);
        }
        private int LastProductId()
        {
            return dataRepository.GetAllProducts().Max(x => x.Key);
        }
        private int LastProductVariantId()
        {
            return dataRepository.GetAllProductVariants().Max(x => x.Id);
        }
        private int LastEventId()
        {
            return dataRepository.GetAllEvents().Max(x => x.Id);
        }

        private bool IdClientInEvents(int idClient)
        {
            if (dataRepository.GetAllEvents().Where(x => x.Client.Id == idClient).ToList().Count > 0)
                return true;
            return false;
        }
        private bool IsProductVariantInEvents(int idVariant)
        {
            if (dataRepository.GetAllEvents().Where(x => x.ProductVariant.Id == idVariant).ToList().Count > 0)
                return true;
            return false;
        }
        private bool IsProductInProductVariant(int idProduct)
        {
            if (dataRepository.GetAllProductVariants().Where(x => x.Product.Id == idProduct).ToList().Count > 0)
                return true;
            return false;
        }
        #endregion


        #region Data modifications
        #region Client
        public bool AddClient(string name, string surname, string email, string address)
        {
            try
            {
                Client cl = new Client(LastClientId() + 1, name, surname, email, address);
                dataRepository.AddClient(cl);
                Console.WriteLine("Client added: {0}", cl.ToString());
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception : {0}", e.Message);
            }
            return false;
        }
        public bool RemoveClient(int clientId)
        {
            try
            {
                Client cl = dataRepository.GetClientById(clientId);
                if (cl != null)
                {
                    if (!IdClientInEvents(clientId))
                    {
                        dataRepository.DeleteClientById(clientId);
                        Console.WriteLine("Client removed: {0}", cl.ToString());
                        return true;
                    }
                    else
                        Console.WriteLine("Can't remove client, client has order(s).");
                }
                else
                    Console.WriteLine("Can't remove client, client with id {0} doesn't exist.", clientId);
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception : {0}", e.Message);
            }
            return false;
        }
        #endregion


        #region Product
        public bool AddProduct(string name, string manufacture, string description)
        {
            try
            {
                Product pr = new Product(LastProductId() + 1, name, manufacture, description);
                dataRepository.AddProduct(pr);
                Console.WriteLine("Product added: {0}", pr.ToString());
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception : {0}", e.Message);
            }
            return false;
        }
        public bool RemoveProduct(int productId)
        {
            try
            {
                Product pr = dataRepository.GetProductById(productId);
                if (!IsProductInProductVariant(productId))
                {
                    dataRepository.DeleteProductById(productId);
                    Console.WriteLine("Product removed: {0}", pr.ToString());
                    return true;
                }
                else
                    Console.WriteLine("Can't remove product, product has product variant(s).");
            }
            catch (KeyNotFoundException)
            {
                Console.WriteLine("Can't remove product, product with id {0} doesn't exist.", productId);
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception : {0}", e.Message);
            }
            return false;
        }
        #endregion


        #region ProductVariant
        public bool AddProductVariant(int productId, double quantity, double price, string description)
        {
            try
            {
                Product product = dataRepository.GetProductById(productId);
                ProductVariant vr = new ProductVariant(LastProductVariantId() + 1, product, quantity, price, description);
                dataRepository.AddProductVariant(vr);
                Console.WriteLine("Product variant added: {0}", vr.ToString());
                return true;
            }
            catch (KeyNotFoundException)
            {
                Console.WriteLine("Can't add variant, product with id {0} doesn't exist.");
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception : {0}", e.Message);
            }
            return false;
        }
        public bool RemoveProductVariant(int variantId)
        {
            try
            {
                ProductVariant vr = dataRepository.GetProductVariantById(variantId);
                if (vr != null)
                {
                    if (!IsProductVariantInEvents(variantId))
                    {
                        dataRepository.DeleteProductVariantById(variantId);
                        Console.WriteLine("Product variant removed: {0}", vr.ToString());
                        return true;
                    }
                    else
                        Console.WriteLine("Can't remove product variant, product variant is used in order(s).");
                }
                else
                    Console.WriteLine("Can't remove product variant, product variant with id {0} doesn't exist.");
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception : {0}", e.Message);
            }
            return false;
        }
        #endregion


        #region Event
        public bool AddOrder(int clientId, int variantId, double quantity)
        {
            try
            {
                Client client = null;
                ProductVariant variant = null;

                try
                {
                    client = dataRepository.GetClientById(clientId);
                    variant = dataRepository.GetProductVariantById(variantId);
                }
                catch (Exception e)
                {
                    Console.WriteLine("Exception : {0}", e.Message);
                }

                if (client == null)
                    Console.WriteLine("Can't add order, client with id {0} doesn't exist.");
                else if (variant == null)
                    Console.WriteLine("Can't add order, product variant with id {0} doesn't exist.");
                else
                {
                    if (quantity > variant.QuantityAvailable)
                    {
                        Console.WriteLine("Can't add order, not enough quantity.  In order: {0}  Available: {1}", quantity, variant.QuantityAvailable);
                        return false;
                    }
                    Order or = new Order(LastEventId() + 1, variant, client, quantity, DateTime.Now);
                    variant.QuantityAvailable -= quantity;
                    dataRepository.AddEvent(or);
                    Console.WriteLine("Order added: {0}", or.ToString());
                    return true;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception : {0}", e.Message);
            }
            return false;
        }
        public bool RemoveEvent(int orderId)
        {
            try
            {
                AEvent ev = dataRepository.GetEventById(orderId);
                if (ev != null)
                {
                    ev.ProductVariant.QuantityAvailable += ev.Quantity;
                    dataRepository.DeleteEventById(orderId);
                    Console.WriteLine("Event removed: {0}", ev.ToString());
                    return true;
                }
                else
                    Console.WriteLine("Can't remove event, event with id {0} doesn't exist.");
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception : {0}", e.Message);
            }
            return false;
        }
        #endregion
        #endregion


        #region Data filter and search
        public void FilterProductsByManufacture(string manufacture)
        {
            try
            {
                Console.WriteLine("Searching for products with manufacture = {0}", manufacture);
                List<Product> products = dataRepository.GetAllProducts().Values.Where(x => x.Manufacture.ToUpper() == manufacture.ToUpper()).ToList();

                if (products.Count == 0)
                    Console.WriteLine("Results not found.");
                else
                {
                    Console.WriteLine("Results found {0}:", products.Count);
                    foreach (Product pr in products)
                        Console.WriteLine(pr.ToString());
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception : {0}", e.Message);
            }
        }
        public void FilterLowQuantityVariants()
        {
            try
            {
                int minQuantity = 2;
                Console.WriteLine("Searching for products variants with quantity < {0}", minQuantity);
                List<ProductVariant> prodVariants = dataRepository.GetAllProductVariants().Where(x => x.QuantityAvailable < minQuantity).ToList();

                if (prodVariants.Count == 0)
                    Console.WriteLine("Results not found.");
                else
                {
                    Console.WriteLine("Results found {0}:", prodVariants.Count);
                    foreach (ProductVariant pr in prodVariants)
                        Console.WriteLine(pr.ToString());
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception : {0}", e.Message);
            }
        }

        public void SearchOrdersBetweenDates(DateTime startDate, DateTime endDate)
        {
            try
            {
                Console.WriteLine("Searching for orders with order date between {0} and {1}", startDate, endDate);
                

                List<AEvent> orders = dataRepository.GetAllEvents().Where(x => x is Order && ((Order)x).OrderDate >= startDate && ((Order)x).OrderDate <= endDate).ToList();

                if (orders.Count == 0)
                    Console.WriteLine("Results not found.");
                else
                {
                    Console.WriteLine("Results found {0}:", orders.Count);
                    foreach (AEvent or in orders)
                        Console.WriteLine(or.ToString());
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception : {0}", e.Message);
            }
        }
        public void SearchVariantsBetweenPrice(double startPrice, double endPrice)
        {
            try
            {
                Console.WriteLine("Searching for products variants with price between {0} and {1}", startPrice, endPrice);
                List<ProductVariant> prodVariants = dataRepository.GetAllProductVariants().Where(x => x.Price >= startPrice && x.Price <= endPrice).ToList();

                if (prodVariants.Count == 0)
                    Console.WriteLine("Results not found.");
                else
                {
                    Console.WriteLine("Results found {0}:", prodVariants.Count);
                    foreach (ProductVariant pr in prodVariants)
                        Console.WriteLine(pr.ToString());
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception : {0}", e.Message);
            }
        }
        #endregion
    }
}
