﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Library
{
    public class Order : AEvent
    {
        public override int Id { get; set; } = -1;
        public override int ProductVariantId { get; set; } = -1;
        [XmlIgnore]
        public override ProductVariant ProductVariant { get; set; } = null;
        public override int ClientId { get; set; } = -1;
        [XmlIgnore]
        public override Client Client { get; set; } = null;
        public override double Quantity { get; set; } = 0.0;
        public DateTime OrderDate { get; set; } = DateTime.Now;

        public Order()
        {

        }

        public Order(int id, ProductVariant productVariant, Client client, double quantity, DateTime orderDate)
        {
            Id = id;
            ProductVariant = productVariant;
            Client = client;
            Quantity = quantity;
            OrderDate = orderDate;
        }
        
        public override string ToString()
        {
            return string.Format("[{0}] Produkt:{1}; Klient:{2} (Ilość:{3}; Data:{4})", Id, ProductVariant.Id, Client.Id, Quantity, OrderDate);
        }

        public override bool Equals(object obj)
        {
            if (obj != null && obj is Order)
            {
                Order clt = obj as Order;
                if (Id == clt.Id)
                    return true;
            }
            return false;
        }
    }
}
