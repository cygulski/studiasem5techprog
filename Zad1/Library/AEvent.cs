﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library
{
    public abstract class AEvent
    {
        public abstract int Id { get; set; }
        public abstract int ProductVariantId { get; set; }
        public abstract ProductVariant ProductVariant { get; set; }
        public abstract int ClientId { get; set; }
        public abstract Client Client { get; set; }
        public abstract double Quantity { get; set; }
    }
}
