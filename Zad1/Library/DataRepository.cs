﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library
{
    public class DataRepository
    {
        protected DataContext dataContext = new DataContext();
        protected IFiller dataFiller = null;

        public event NotifyCollectionChangedEventHandler ProductsVariantsChanged;
        public event NotifyCollectionChangedEventHandler EventsChanged;

        public IFiller Filler
        {
            set
            {
                if (value != null)
                {
                    dataFiller = value;
                    value.FillData(dataContext);
                }
            }
        }

        public DataRepository()
        {
            dataContext.ProductsVariants.CollectionChanged += ProductsVariants_CollectionChanged;
            dataContext.Events.CollectionChanged += Events_CollectionChanged;
        }
        public DataRepository(IFiller filler)
        {
            dataContext.ProductsVariants.CollectionChanged += ProductsVariants_CollectionChanged;
            dataContext.Events.CollectionChanged += Events_CollectionChanged;

            dataFiller = filler;
            filler.FillData(dataContext);
        }
        
        private void Events_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            EventsChanged?.Invoke(sender, e);
        }
        private void ProductsVariants_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            ProductsVariantsChanged?.Invoke(sender, e);
        }

        #region Client
        public void AddClient(Client client)
        {
            if (client != null)
                dataContext.Clients.Add(client);
            else
                throw new ArgumentNullException();
        }
        public Client GetClientAtPosition(int position)
        {
            // throws ArgumentOutOfRangeException
            return dataContext.Clients[position];
        }
        public Client GetClientById(int id)
        {
            if (dataContext.Clients.Exists(x => x.Id == id))
                return dataContext.Clients.First(x => x.Id == id);
            else
                return null;
        }
        public List<Client> GetAllClients()
        {
            return dataContext.Clients;
        }
        public void UpdateClientAtPosition(int position, Client newClientData)
        {
            if (newClientData != null)
            {
                // throws ArgumentOutOfRangeException
                Client client = dataContext.Clients[position];
                if (client != null)
                {
                    client.Address = newClientData.Address;
                    client.Email = newClientData.Email;
                    client.Id = newClientData.Id;
                    client.Name = newClientData.Name;
                    client.Surname = newClientData.Surname;
                }
                else
                    throw new NullReferenceException();
            }
            else
                throw new ArgumentNullException();
        }
        public bool UpdateClientById(int id, Client newClientData)
        {
            if (newClientData != null)
            {
                if (dataContext.Clients.Exists(x => x.Id == id))
                {
                    Client client = dataContext.Clients.First(x => x.Id == id);
                    client.Address = newClientData.Address;
                    client.Email = newClientData.Email;
                    client.Name = newClientData.Name;
                    client.Surname = newClientData.Surname;
                    return true;
                }
                return false;
            }
            else
                throw new ArgumentNullException();
        }
        public void DeleteClientAtPosition(int position)
        {
            // throws ArgumentOutOfRangeException
            dataContext.Clients.RemoveAt(position);
        }
        public bool DeleteClientById(int id)
        {
            if (dataContext.Clients.Exists(x => x.Id == id))
            {
                Client client = dataContext.Clients.First(x => x.Id == id);
                return dataContext.Clients.Remove(client);
            }
            return false;
        }
        #endregion
        
        #region Product
        public void AddProduct(Product product)
        {
            if (product != null)
            {
                // throws ArgumentException if duplicated key
                dataContext.Products.Add(product.Id, product);
            }
            else
                throw new ArgumentNullException();
        }
        public Product GetProductById(int id)
        {
            // throws KeyNotFoundException
            return dataContext.Products[id];
        }
        public Dictionary<int, Product> GetAllProducts()
        {
            return dataContext.Products;
        }
        public void UpdateProductById(int id, Product newProductData)
        {
            if (newProductData != null)
            {
                // throws KeyNotFoundException
                Product product = dataContext.Products[id];
                if (product != null)
                {
                    product.Description = newProductData.Description;
                    product.Manufacture = newProductData.Manufacture;
                    product.Name = newProductData.Name;
                }
                else
                    throw new NullReferenceException();
            }
            else
                throw new ArgumentNullException();
        }
        public bool DeleteProductById(int id)
        {
            return dataContext.Products.Remove(id);
        }
        #endregion

        #region ProductVariant
        public void AddProductVariant(ProductVariant productVariant)
        {
            if (productVariant != null)
                dataContext.ProductsVariants.Add(productVariant);
            else
                throw new ArgumentNullException();
        }
        public ProductVariant GetProductVariantAtPosition(int position)
        {
            // throws ArgumentOutOfRangeException
            return dataContext.ProductsVariants[position];
        }
        public ProductVariant GetProductVariantById(int id)
        {
            if (dataContext.ProductsVariants.Any(x => x.Id == id))
                return dataContext.ProductsVariants.First(x => x.Id == id);
            else
                return null;
        }
        public ObservableCollection<ProductVariant> GetAllProductVariants()
        {
            return dataContext.ProductsVariants;
        }
        public void UpdateProductVariantAtPosition(int position, ProductVariant newProductVariantData)
        {
            if (newProductVariantData != null)
            {
                // throws ArgumentOutOfRangeException
                ProductVariant productVariant = dataContext.ProductsVariants[position];
                if (productVariant != null)
                {
                    productVariant.Id = newProductVariantData.Id;
                    productVariant.Price = newProductVariantData.Price;
                    productVariant.Product = newProductVariantData.Product;
                    productVariant.QuantityAvailable = newProductVariantData.QuantityAvailable;
                    productVariant.VariantDescription = newProductVariantData.VariantDescription;
                }
                else
                    throw new NullReferenceException();
            }
            else
                throw new ArgumentNullException();
        }
        public bool UpdateProductVariantById(int id, ProductVariant newProductVariantData)
        {
            if (newProductVariantData != null)
            {
                if (dataContext.ProductsVariants.Any(x => x.Id == id))
                {
                    ProductVariant productVariant = dataContext.ProductsVariants.First(x => x.Id == id);
                    productVariant.Price = newProductVariantData.Price;
                    productVariant.Product = newProductVariantData.Product;
                    productVariant.QuantityAvailable = newProductVariantData.QuantityAvailable;
                    productVariant.VariantDescription = newProductVariantData.VariantDescription;
                    return true;
                }
                return false;
            }
            else
                throw new ArgumentNullException();
        }
        public void DeleteProductVariantAtPosition(int position)
        {
            // throws ArgumentOutOfRangeException
            dataContext.ProductsVariants.RemoveAt(position);
        }
        public bool DeleteProductVariantById(int id)
        {
            if (dataContext.ProductsVariants.Any(x => x.Id == id))
            {
                ProductVariant productVariant = dataContext.ProductsVariants.First(x => x.Id == id);
                return dataContext.ProductsVariants.Remove(productVariant);
            }
            return false;
        }
        #endregion
        
        #region Events
        public void AddEvent(AEvent even)
        {
            if (even != null)
                dataContext.Events.Add(even);
            else
                throw new ArgumentNullException();
        }
        public AEvent GetEventAtPosition(int position)
        {
            // throws ArgumentOutOfRangeException
            return dataContext.Events[position];
        }
        public AEvent GetEventById(int id)
        {
            if (dataContext.Events.Any(x => x.Id == id))
                return dataContext.Events.First(x => x.Id == id);
            else
                return null;
        }
        public ObservableCollection<AEvent> GetAllEvents()
        {
            return dataContext.Events;
        }
        public void UpdateEventAtPosition(int position, AEvent newEventData)
        {
            if (newEventData != null)
            {
                // throws ArgumentOutOfRangeException
                AEvent even = dataContext.Events[position];
                if (even != null)
                {
                    even.Client = newEventData.Client;
                    even.Id = newEventData.Id;
                    even.ProductVariant = newEventData.ProductVariant;
                    even.Quantity = newEventData.Quantity;
                }
                else
                    throw new NullReferenceException();
            }
            else
                throw new ArgumentNullException();
        }
        public bool UpdateEventById(int id, AEvent newEventData)
        {
            if (newEventData != null)
            {
                if (dataContext.Events.Any(x => x.Id == id))
                {
                    AEvent even = dataContext.Events.First(x => x.Id == id);
                    even.Client = newEventData.Client;
                    even.ProductVariant = newEventData.ProductVariant;
                    even.Quantity = newEventData.Quantity;
                    return true;
                }
                return false;
            }
            else
                throw new ArgumentNullException();
        }
        public void DeleteEventAtPosition(int position)
        {
            // throws ArgumentOutOfRangeException
            dataContext.Events.RemoveAt(position);
        }
        public bool DeleteEventById(int id)
        {
            if (dataContext.Events.Any(x => x.Id == id))
            {
                AEvent even = dataContext.Events.First(x => x.Id == id);
                return dataContext.Events.Remove(even);
            }
            return false;
        }
        #endregion
    }
}
