﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Library
{
    public class ProductVariant
    {
        public int Id { get; set; } = -1;
        public int ProductId { get; set; } = -1;
        [XmlIgnore]
        public Product Product { get; set; } = null;
        public double QuantityAvailable { get; set; } = 0.0;
        public double Price { get; set; } = 0.0;
        public string VariantDescription { get; set; } = "";

        public ProductVariant()
        {

        }

        public ProductVariant(int id, Product product, double quantityAvailable, double price, string variantDecription)
        {
            Id = id;
            Product = product;
            QuantityAvailable = quantityAvailable;
            Price = price;
            VariantDescription = variantDecription;
        }

        public override string ToString()
        {
            return string.Format("[{0}] Produkt:{1} (Ilość: {2}; Cena: {3} ; {4})", Id, Product.Id, QuantityAvailable, Price, VariantDescription);
        }

        public override bool Equals(object obj)
        {
            if (obj != null && obj is ProductVariant)
            {
                ProductVariant vnt = obj as ProductVariant;
                if (Id == vnt.Id)
                    return true;
            }
            return false;
        }
    }
}
