﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Library;
using System.Collections.Generic;

namespace Tests
{
    [TestClass]
    public class DataRepositoryTests
    {
        #region -- Client ----------------------------------------------

        #region ClientAdd
        [TestMethod]
        public void ClientAdd()
        {
            DataRepository rep = new DataRepository(new StaticFiller());
            Assert.AreEqual(3, rep.GetAllClients().Count);
            Client client = new Client(5, "Test", "Test", "test@test.test", "00-000 Test, Testowa 0");
            rep.AddClient(client);
            Assert.AreEqual(4, rep.GetAllClients().Count);
            Assert.AreEqual(client, rep.GetClientById(5));
        }
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ClientAdd_ArgumentNull()
        {
            DataRepository rep = new DataRepository(new StaticFiller());
            rep.AddClient(null);
        }
        #endregion

        #region ClientGetAtPosition
        [TestMethod]
        public void ClientGetAtPosition()
        {
            DataRepository rep = new DataRepository(new StaticFiller());
            Assert.AreEqual(rep.GetAllClients()[0], rep.GetClientAtPosition(0));
        }
        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void ClientGetAtPosition_ArgumentOutOfRange_Smaller()
        {
            DataRepository rep = new DataRepository(new StaticFiller());
            rep.GetClientAtPosition(-1);
        }
        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void ClientGetAtPosition_ArgumentOutOfRange_Bigger()
        {
            DataRepository rep = new DataRepository(new StaticFiller());
            rep.GetClientAtPosition(100);
        }
        #endregion

        #region ClientGetById
        [TestMethod]
        public void ClientGetById()
        {
            DataRepository rep = new DataRepository(new StaticFiller());
            Client client = new Client(5, "Test", "Test", "test@test.test", "00-000 Test, Testowa 0");
            rep.AddClient(client);
            Assert.AreEqual(client, rep.GetClientById(5));
        }
        [TestMethod]
        public void ClientGetById_ArgumentOutOfRange_Smaller()
        {
            DataRepository rep = new DataRepository(new StaticFiller());
            Assert.AreEqual(null, rep.GetClientById(-1));
        }
        [TestMethod]
        public void ClientGetById_ArgumentOutOfRange_Bigger()
        {
            DataRepository rep = new DataRepository(new StaticFiller());
            Assert.AreEqual(null, rep.GetClientById(100));
        }
        #endregion

        #region ClientsGetAll
        [TestMethod]
        public void ClientsGetAll()
        {
            DataRepository rep = new DataRepository(new StaticFiller());
            Assert.AreEqual(3, rep.GetAllClients().Count);
            Client client = new Client(5, "Test", "Test", "test@test.test", "00-000 Test, Testowa 0");
            rep.AddClient(client);
            Assert.AreEqual(4, rep.GetAllClients().Count);
        }
        #endregion

        #region ClientUpdateAtPosition
        [TestMethod]
        public void ClientUpdateAtPosition()
        {
            DataRepository rep = new DataRepository(new StaticFiller());

            Client newClient = new Client(5, "Test", "Test", "test@test.test", "00-000 Test, Testowa 0");
            Client client = rep.GetClientAtPosition(0);

            Assert.AreNotEqual(client.Id, newClient.Id);
            Assert.AreNotEqual(client.Address, newClient.Address);
            Assert.AreNotEqual(client.Email, newClient.Email);
            Assert.AreNotEqual(client.Name, newClient.Name);
            Assert.AreNotEqual(client.Surname, newClient.Surname);

            rep.UpdateClientAtPosition(0, newClient);

            Assert.AreEqual(client.Id, newClient.Id);
            Assert.AreEqual(client.Address, newClient.Address);
            Assert.AreEqual(client.Email, newClient.Email);
            Assert.AreEqual(client.Name, newClient.Name);
            Assert.AreEqual(client.Surname, newClient.Surname);
        }
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ClientUpdateAtPosition_ArgumentNull()
        {
            DataRepository rep = new DataRepository(new StaticFiller());
            rep.UpdateClientAtPosition(0, null);
        }
        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void ClientUpdateAtPosition_ArgumentOutOfRange_Smaller()
        {
            DataRepository rep = new DataRepository(new StaticFiller());
            rep.UpdateClientAtPosition(-1, new Client(5, "Test", "Test", "test@test.test", "00-000 Test, Testowa 0"));
        }
        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void ClientUpdateAtPosition_ArgumentOutOfRange_Bigger()
        {
            DataRepository rep = new DataRepository(new StaticFiller());
            rep.UpdateClientAtPosition(100, new Client(5, "Test", "Test", "test@test.test", "00-000 Test, Testowa 0"));
        }
        #endregion

        #region ClientUpdateById
        [TestMethod]
        public void ClientUpdateById()
        {
            DataRepository rep = new DataRepository(new StaticFiller());

            Client newClient = new Client(5, "Test", "Test", "test@test.test", "00-000 Test, Testowa 0");
            Client client = rep.GetClientAtPosition(0);

            Assert.AreNotEqual(client.Id, newClient.Id);
            Assert.AreNotEqual(client.Address, newClient.Address);
            Assert.AreNotEqual(client.Email, newClient.Email);
            Assert.AreNotEqual(client.Name, newClient.Name);
            Assert.AreNotEqual(client.Surname, newClient.Surname);

            Assert.AreEqual(rep.UpdateClientById(0, newClient), true);

            Assert.AreNotEqual(client.Id, newClient.Id);
            Assert.AreEqual(client.Address, newClient.Address);
            Assert.AreEqual(client.Email, newClient.Email);
            Assert.AreEqual(client.Name, newClient.Name);
            Assert.AreEqual(client.Surname, newClient.Surname);
        }
        [TestMethod]
        public void ClientUpdateById_ArgumentOutOfRange_Smaller()
        {
            DataRepository rep = new DataRepository(new StaticFiller());
            Client newClient = new Client(5, "Test", "Test", "test@test.test", "00-000 Test, Testowa 0");
            Assert.AreEqual(false, rep.UpdateClientById(-1, newClient));
        }
        [TestMethod]
        public void ClientUpdateById_ArgumentOutOfRange_Bigger()
        {
            DataRepository rep = new DataRepository(new StaticFiller());
            Client newClient = new Client(5, "Test", "Test", "test@test.test", "00-000 Test, Testowa 0");
            Assert.AreEqual(false, rep.UpdateClientById(100, newClient));
        }
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ClientUpdateById_ArgumentNull()
        {
            DataRepository rep = new DataRepository(new StaticFiller());
            rep.UpdateClientById(0, null);
        }
        #endregion

        #region ClientDeleteAtPosition
        [TestMethod]
        public void ClientDeleteAtPosition()
        {
            DataRepository rep = new DataRepository(new StaticFiller());
            Assert.AreEqual(3, rep.GetAllClients().Count);
            int idToDelete = rep.GetClientAtPosition(0).Id;
            rep.DeleteClientAtPosition(0);
            Assert.AreEqual(2, rep.GetAllClients().Count);
            Assert.AreEqual(null, rep.GetClientById(idToDelete));
        }
        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void ClientDeleteAtPosition_ArgumentOutOfRange_Smaller()
        {
            DataRepository rep = new DataRepository(new StaticFiller());
            rep.DeleteClientAtPosition(-1);
        }
        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void ClientDeleteAtPosition_ArgumentOutOfRange_Bigger()
        {
            DataRepository rep = new DataRepository(new StaticFiller());
            rep.DeleteClientAtPosition(100);
        }
        #endregion

        #region ClientDeleteById
        [TestMethod]
        public void ClientDeleteById()
        {
            DataRepository rep = new DataRepository(new StaticFiller());
            Assert.AreEqual(3, rep.GetAllClients().Count);
            Assert.AreEqual(true, rep.DeleteClientById(0));
            Assert.AreEqual(2, rep.GetAllClients().Count);
            Assert.AreEqual(null, rep.GetClientById(0));
        }
        [TestMethod]
        public void ClientDeleteById_ArgumentOutOfRange_Smaller()
        {
            DataRepository rep = new DataRepository(new StaticFiller());
            Assert.AreEqual(false, rep.DeleteClientById(-1));
        }
        [TestMethod]
        public void ClientDeleteById_ArgumentOutOfRange_Bigger()
        {
            DataRepository rep = new DataRepository(new StaticFiller());
            Assert.AreEqual(false, rep.DeleteClientById(100));
        }
        #endregion

        #endregion


        #region -- Product ---------------------------------------------

        #region ProductAdd
        [TestMethod]
        public void ProductAdd()
        {
            DataRepository rep = new DataRepository(new StaticFiller());

            Assert.AreEqual(3, rep.GetAllProducts().Count);
            Product product = new Product(5, "Test", "Test", "Test");
            rep.AddProduct(product);
            Assert.AreEqual(4, rep.GetAllProducts().Count);
            Assert.AreEqual(product, rep.GetProductById(5));
        }
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ProductAdd_ArgumentNull()
        {
            DataRepository rep = new DataRepository(new StaticFiller());
            rep.AddProduct(null);
        }
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void ProductAdd_Argument()
        {
            DataRepository rep = new DataRepository(new StaticFiller());
            rep.AddProduct(rep.GetProductById(0));
        }
        #endregion

        #region ProductGetById
        [TestMethod]
        public void ProductGetById()
        {
            DataRepository rep = new DataRepository(new StaticFiller());
            Product product = new Product(5, "Test", "Test", "Test");
            rep.AddProduct(product);
            Assert.AreEqual(product, rep.GetProductById(5));
        }
        [TestMethod]
        [ExpectedException(typeof(KeyNotFoundException))]
        public void ProductGetById_KeyNotFound_Smaller()
        {
            DataRepository rep = new DataRepository(new StaticFiller());
            rep.GetProductById(-1);
        }
        [TestMethod]
        [ExpectedException(typeof(KeyNotFoundException))]
        public void ProductGetById_KeyNotFound_Bigger()
        {
            DataRepository rep = new DataRepository(new StaticFiller());
            rep.GetProductById(100);
        }
        #endregion

        #region ProductsGetAll
        [TestMethod]
        public void ProductsGetAll()
        {
            DataRepository rep = new DataRepository(new StaticFiller());

            Assert.AreEqual(3, rep.GetAllProducts().Count);
            Product product = new Product(5, "Test", "Test", "Test");
            rep.AddProduct(product);
            Assert.AreEqual(4, rep.GetAllProducts().Count);
        }
        #endregion

        #region ProductUpdateById
        [TestMethod]
        public void ProductUpdateById()
        {
            DataRepository rep = new DataRepository(new StaticFiller());

            Product newProduct = new Product(5, "Test", "Test", "Test");
            Product product = rep.GetProductById(0);

            Assert.AreNotEqual(product.Id, newProduct.Id);
            Assert.AreNotEqual(product.Description, newProduct.Description);
            Assert.AreNotEqual(product.Manufacture, newProduct.Manufacture);
            Assert.AreNotEqual(product.Name, newProduct.Name);

            rep.UpdateProductById(0, newProduct);

            Assert.AreNotEqual(product.Id, newProduct.Id);
            Assert.AreEqual(product.Description, newProduct.Description);
            Assert.AreEqual(product.Manufacture, newProduct.Manufacture);
            Assert.AreEqual(product.Name, newProduct.Name);
        }
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ProductUpdateById_ArgumentNull()
        {
            DataRepository rep = new DataRepository(new StaticFiller());
            rep.UpdateProductById(0, null);
        }
        [TestMethod]
        [ExpectedException(typeof(KeyNotFoundException))]
        public void ProductUpdateById_KeyNotFound_Smaller()
        {
            DataRepository rep = new DataRepository(new StaticFiller());
            Product product = new Product(5, "Test", "Test", "Test");
            rep.UpdateProductById(-1, product);
        }
        [TestMethod]
        [ExpectedException(typeof(KeyNotFoundException))]
        public void ProductUpdateById_KeyNotFound_Bigger()
        {
            DataRepository rep = new DataRepository(new StaticFiller());
            Product product = new Product(5, "Test", "Test", "Test");
            rep.UpdateProductById(100, product);
        }
        #endregion

        #region ProductDeleteById
        [TestMethod]
        public void ProductDeleteById()
        {
            DataRepository rep = new DataRepository(new StaticFiller());
        }
        [TestMethod]
        public void ProductDeleteById_ArgumentOutOfRange_Smaller()
        {
            DataRepository rep = new DataRepository(new StaticFiller());
            Assert.AreEqual(false, rep.DeleteProductById(-1));
        }
        [TestMethod]
        public void ProductDeleteById_ArgumentOutOfRange_Bigger()
        {
            DataRepository rep = new DataRepository(new StaticFiller());
            Assert.AreEqual(false, rep.DeleteProductById(100));
        }
        #endregion

        #endregion


        #region -- Product Variant -------------------------------------

        #region ProductVariantAdd
        [TestMethod]
        public void ProductVariantAdd()
        {
            DataRepository rep = new DataRepository(new StaticFiller());
            Assert.AreEqual(3, rep.GetAllProductVariants().Count);
            Product product = new Product(5, "Test", "Test", "Test");
            ProductVariant productVariant = new ProductVariant(5, product, 3.0, 30.0, "Test");
            rep.AddProductVariant(productVariant);
            Assert.AreEqual(4, rep.GetAllProductVariants().Count);
            Assert.AreEqual(productVariant, rep.GetProductVariantById(5));
        }
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ProductVariantAdd_ArgumentNull()
        {
            DataRepository rep = new DataRepository(new StaticFiller());
            rep.AddProductVariant(null);
        }
        #endregion

        #region ProductVariantGetAtPosition
        [TestMethod]
        public void ProductVariantGetAtPosition()
        {
            DataRepository rep = new DataRepository(new StaticFiller());
            Assert.AreEqual(rep.GetAllProductVariants()[0], rep.GetProductVariantAtPosition(0));
        }
        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void ProductVariantGetAtPosition_ArgumentOutOfRange_Smaller()
        {
            DataRepository rep = new DataRepository(new StaticFiller());
            rep.GetProductVariantAtPosition(-1);
        }
        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void ProductVariantGetAtPosition_ArgumentOutOfRange_Bigger()
        {
            DataRepository rep = new DataRepository(new StaticFiller());
            rep.GetProductVariantAtPosition(100);
        }
        #endregion

        #region ProductVariantGetById
        [TestMethod]
        public void ProductVariantGetById()
        {
            DataRepository rep = new DataRepository(new StaticFiller());
            Product product = new Product(5, "Test", "Test", "Test");
            ProductVariant productVariant = new ProductVariant(5, product, 3.0, 30.0, "Test");
            rep.AddProductVariant(productVariant);
            Assert.AreEqual(productVariant, rep.GetProductVariantById(5));
        }
        [TestMethod]
        public void ProductVariantGetById_ArgumentOutOfRange_Smaller()
        {
            DataRepository rep = new DataRepository(new StaticFiller());
            Assert.AreEqual(null, rep.GetProductVariantById(-1));
        }
        [TestMethod]
        public void ProductVariantGetById_ArgumentOutOfRange_Bigger()
        {
            DataRepository rep = new DataRepository(new StaticFiller());
            Assert.AreEqual(null, rep.GetProductVariantById(100));
        }
        #endregion

        #region ProductVariantsGetAll
        [TestMethod]
        public void ProductVariantsGetAll()
        {
            DataRepository rep = new DataRepository(new StaticFiller());
            Assert.AreEqual(3, rep.GetAllProductVariants().Count);
            Product product = new Product(5, "Test", "Test", "Test");
            ProductVariant productVariant = new ProductVariant(5, product, 3.0, 30.0, "Test");
            rep.AddProductVariant(productVariant);
            Assert.AreEqual(4, rep.GetAllProductVariants().Count);
        }
        #endregion

        #region ProductVariantUpdateAtPosition
        [TestMethod]
        public void ProductVariantUpdateAtPosition()
        {
            DataRepository rep = new DataRepository(new StaticFiller());

            Product product = new Product(5, "Test", "Test", "Test");
            ProductVariant newProductVariant = new ProductVariant(5, product, 3.0, 30.0, "Test");
            ProductVariant productVariant = rep.GetProductVariantAtPosition(0);

            Assert.AreNotEqual(productVariant.Id, newProductVariant.Id);
            Assert.AreNotEqual(productVariant.Price, newProductVariant.Price);
            Assert.AreNotEqual(productVariant.Product, newProductVariant.Product);
            Assert.AreNotEqual(productVariant.QuantityAvailable, newProductVariant.QuantityAvailable);
            Assert.AreNotEqual(productVariant.VariantDescription, newProductVariant.VariantDescription);

            rep.UpdateProductVariantAtPosition(0, newProductVariant);

            Assert.AreEqual(productVariant.Id, newProductVariant.Id);
            Assert.AreEqual(productVariant.Price, newProductVariant.Price);
            Assert.AreEqual(productVariant.Product, newProductVariant.Product);
            Assert.AreEqual(productVariant.QuantityAvailable, newProductVariant.QuantityAvailable);
            Assert.AreEqual(productVariant.VariantDescription, newProductVariant.VariantDescription);
        }
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ProductVariantUpdateAtPosition_ArgumentNull()
        {
            DataRepository rep = new DataRepository(new StaticFiller());
            rep.UpdateProductVariantAtPosition(0, null);
        }
        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void ProductVariantUpdateAtPosition_ArgumentOutOfRange_Smaller()
        {
            DataRepository rep = new DataRepository(new StaticFiller());
            Product product = new Product(5, "Test", "Test", "Test");
            ProductVariant productVariant = new ProductVariant(5, product, 3.0, 30.0, "Test");
            rep.UpdateProductVariantAtPosition(-1, productVariant);
        }
        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void ProductVariantUpdateAtPosition_ArgumentOutOfRange_Bigger()
        {
            DataRepository rep = new DataRepository(new StaticFiller());
            Product product = new Product(5, "Test", "Test", "Test");
            ProductVariant productVariant = new ProductVariant(5, product, 3.0, 30.0, "Test");
            rep.UpdateProductVariantAtPosition(100, productVariant);
        }
        #endregion

        #region ProductVariantUpdateById
        [TestMethod]
        public void ProductVariantUpdateById()
        {
            DataRepository rep = new DataRepository(new StaticFiller());

            Product product = new Product(5, "Test", "Test", "Test");
            ProductVariant newProductVariant = new ProductVariant(5, product, 3.0, 30.0, "Test");
            ProductVariant productVariant = rep.GetProductVariantAtPosition(0);

            Assert.AreNotEqual(productVariant.Id, newProductVariant.Id);
            Assert.AreNotEqual(productVariant.Price, newProductVariant.Price);
            Assert.AreNotEqual(productVariant.Product, newProductVariant.Product);
            Assert.AreNotEqual(productVariant.QuantityAvailable, newProductVariant.QuantityAvailable);
            Assert.AreNotEqual(productVariant.VariantDescription, newProductVariant.VariantDescription);

            Assert.AreEqual(rep.UpdateProductVariantById(0, newProductVariant), true);

            Assert.AreNotEqual(productVariant.Id, newProductVariant.Id);
            Assert.AreEqual(productVariant.Price, newProductVariant.Price);
            Assert.AreEqual(productVariant.Product, newProductVariant.Product);
            Assert.AreEqual(productVariant.QuantityAvailable, newProductVariant.QuantityAvailable);
            Assert.AreEqual(productVariant.VariantDescription, newProductVariant.VariantDescription);
        }
        [TestMethod]
        public void ProductVariantUpdateById_ArgumentOutOfRange_Smaller()
        {
            DataRepository rep = new DataRepository(new StaticFiller());
            Product product = new Product(5, "Test", "Test", "Test");
            ProductVariant productVariant = new ProductVariant(5, product, 3.0, 30.0, "Test");
            Assert.AreEqual(false, rep.UpdateProductVariantById(-1, productVariant));
        }
        [TestMethod]
        public void ProductVariantUpdateById_ArgumentOutOfRange_Bigger()
        {
            DataRepository rep = new DataRepository(new StaticFiller());
            Product product = new Product(5, "Test", "Test", "Test");
            ProductVariant productVariant = new ProductVariant(5, product, 3.0, 30.0, "Test");
            Assert.AreEqual(false, rep.UpdateProductVariantById(100, productVariant));
        }
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ProductVariantUpdateById_ArgumentNull()
        {
            DataRepository rep = new DataRepository(new StaticFiller());
            rep.UpdateProductVariantById(0, null);
        }
        #endregion

        #region ProductVariantDeleteAtPosition
        [TestMethod]
        public void ProductVariantDeleteAtPosition()
        {
            DataRepository rep = new DataRepository(new StaticFiller());
            Assert.AreEqual(3, rep.GetAllProductVariants().Count);
            int idToDelete = rep.GetProductVariantAtPosition(0).Id;
            rep.DeleteProductVariantAtPosition(0);
            Assert.AreEqual(2, rep.GetAllProductVariants().Count);
            Assert.AreEqual(null, rep.GetProductVariantById(idToDelete));
        }
        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void ProductVariantDeleteAtPosition_ArgumentOutOfRange_Smaller()
        {
            DataRepository rep = new DataRepository(new StaticFiller());
            rep.DeleteProductVariantAtPosition(-1);
        }
        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void ProductVariantDeleteAtPosition_ArgumentOutOfRange_Bigger()
        {
            DataRepository rep = new DataRepository(new StaticFiller());
            rep.DeleteProductVariantAtPosition(100);
        }
        #endregion

        #region ProductVariantDeleteById
        [TestMethod]
        public void ProductVariantDeleteById()
        {
            DataRepository rep = new DataRepository(new StaticFiller());
            Assert.AreEqual(3, rep.GetAllProductVariants().Count);
            Assert.AreEqual(true, rep.DeleteProductVariantById(0));
            Assert.AreEqual(2, rep.GetAllProductVariants().Count);
            Assert.AreEqual(null, rep.GetProductVariantById(0));
        }
        [TestMethod]
        public void ProductVariantDeleteById_ArgumentOutOfRange_Smaller()
        {
            DataRepository rep = new DataRepository(new StaticFiller());
            Assert.AreEqual(false, rep.DeleteProductVariantById(-1));
        }
        [TestMethod]
        public void ProductVariantDeleteById_ArgumentOutOfRange_Bigger()
        {
            DataRepository rep = new DataRepository(new StaticFiller());
            Assert.AreEqual(false, rep.DeleteProductVariantById(100));
        }
        #endregion

        #endregion


        #region -- Event -----------------------------------------------

        #region OrderAdd
        [TestMethod]
        public void EventAdd()
        {
            DataRepository rep = new DataRepository(new StaticFiller());
            Assert.AreEqual(5, rep.GetAllEvents().Count);
            Product product = new Product(5, "Test", "Test", "Test");
            ProductVariant productVariant = new ProductVariant(5, product, 3.0, 30.0, "Test");
            Client client = new Client(5, "Test", "Test", "test@test.test", "00-000 Test, Testowa 0");
            Order order = new Order(5, productVariant, client, 5, DateTime.Now);
            rep.AddEvent(order);
            Assert.AreEqual(6, rep.GetAllEvents().Count);
            Assert.AreEqual(order, rep.GetEventById(5));
        }
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void EventAdd_ArgumentNull()
        {
            DataRepository rep = new DataRepository(new StaticFiller());
            rep.AddEvent(null);
        }
        #endregion

        #region EventAtPositionGet
        [TestMethod]
        public void EventAtPositionGet()
        {
            DataRepository rep = new DataRepository(new StaticFiller());
            Assert.AreEqual(rep.GetAllEvents()[0], rep.GetEventAtPosition(0));
        }
        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void EventAtPositionGet_ArgumentOutOfRange_Smaller()
        {
            DataRepository rep = new DataRepository(new StaticFiller());
            rep.GetEventAtPosition(-1);
        }
        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void EventAtPositionGet_ArgumentOutOfRange_Bigger()
        {
            DataRepository rep = new DataRepository(new StaticFiller());
            rep.GetEventAtPosition(100);
        }
        #endregion

        #region EventGetById
        [TestMethod]
        public void EventGetById()
        {
            DataRepository rep = new DataRepository(new StaticFiller());
            Product product = new Product(5, "Test", "Test", "Test");
            ProductVariant productVariant = new ProductVariant(5, product, 3.0, 30.0, "Test");
            Client client = new Client(5, "Test", "Test", "test@test.test", "00-000 Test, Testowa 0");
            Order order = new Order(5, productVariant, client, 5, DateTime.Now);
            rep.AddEvent(order);
            Assert.AreEqual(rep.GetEventById(5), order);
        }
        [TestMethod]
        public void EventGetById_ArgumentOutOfRange_Smaller()
        {
            DataRepository rep = new DataRepository(new StaticFiller());
            Assert.AreEqual(null, rep.GetEventById(-1));
        }
        [TestMethod]
        public void EventGetById_ArgumentOutOfRange_Bigger()
        {
            DataRepository rep = new DataRepository(new StaticFiller());
            Assert.AreEqual(null, rep.GetEventById(100));
        }
        #endregion

        #region EventGetAll
        [TestMethod]
        public void EventGetAll()
        {
            DataRepository rep = new DataRepository(new StaticFiller());
            Assert.AreEqual(5, rep.GetAllEvents().Count);

            Product product = new Product(5, "Test", "Test", "Test");
            ProductVariant productVariant = new ProductVariant(5, product, 3.0, 30.0, "Test");
            Client client = new Client(5, "Test", "Test", "test@test.test", "00-000 Test, Testowa 0");
            Order order = new Order(5, productVariant, client, 5, DateTime.Now);

            rep.AddEvent(order);
            Assert.AreEqual(6, rep.GetAllEvents().Count);
        }
        #endregion

        #region EventUpdateAtPosition
        [TestMethod]
        public void EventUpdateAtPosition()
        {
            DataRepository rep = new DataRepository(new StaticFiller());

            Product product = new Product(5, "Test", "Test", "Test");
            ProductVariant productVariant = new ProductVariant(5, product, 3.0, 30.0, "Test");
            Client client = new Client(5, "Test", "Test", "test@test.test", "00-000 Test, Testowa 0");
            Order newOrder = new Order(5, productVariant, client, 25, DateTime.Now);
            AEvent even = rep.GetEventAtPosition(0);

            Assert.AreNotEqual(even.Id, newOrder.Id);
            Assert.AreNotEqual(even.Client, newOrder.Client);
            Assert.AreNotEqual(even.ProductVariant, newOrder.ProductVariant);
            Assert.AreNotEqual(even.Quantity, newOrder.Quantity);

            rep.UpdateEventAtPosition(0, newOrder);

            Assert.AreEqual(even.Id, newOrder.Id);
            Assert.AreEqual(even.Client, newOrder.Client);
            Assert.AreEqual(even.ProductVariant, newOrder.ProductVariant);
            Assert.AreEqual(even.Quantity, newOrder.Quantity);
        }
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void EventUpdateAtPosition_ArgumentNull()
        {
            DataRepository rep = new DataRepository(new StaticFiller());
            rep.UpdateEventAtPosition(0, null);
        }
        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void EventUpdateAtPosition_ArgumentOutOfRange_Smaller()
        {
            DataRepository rep = new DataRepository(new StaticFiller());
            Product product = new Product(5, "Test", "Test", "Test");
            ProductVariant productVariant = new ProductVariant(5, product, 3.0, 30.0, "Test");
            Client client = new Client(5, "Test", "Test", "test@test.test", "00-000 Test, Testowa 0");
            Order order = new Order(5, productVariant, client, 5, DateTime.Now);
            rep.UpdateEventAtPosition(-1, order);
        }
        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void EventUpdateAtPosition_ArgumentOutOfRange_Bigger()
        {
            DataRepository rep = new DataRepository(new StaticFiller());
            Product product = new Product(5, "Test", "Test", "Test");
            ProductVariant productVariant = new ProductVariant(5, product, 3.0, 30.0, "Test");
            Client client = new Client(5, "Test", "Test", "test@test.test", "00-000 Test, Testowa 0");
            Order order = new Order(5, productVariant, client, 5, DateTime.Now);
            rep.UpdateEventAtPosition(100, order);
        }
        #endregion

        #region EventUpdateById
        [TestMethod]
        public void EventUpdateById()
        {
            DataRepository rep = new DataRepository(new StaticFiller());

            Product product = new Product(5, "Test", "Test", "Test");
            ProductVariant productVariant = new ProductVariant(5, product, 3.0, 30.0, "Test");
            Client client = new Client(5, "Test", "Test", "test@test.test", "00-000 Test, Testowa 0");
            Order newOrder = new Order(5, productVariant, client, 25, DateTime.Now);
            AEvent even = rep.GetEventById(0);

            Assert.AreNotEqual(even.Id, newOrder.Id);
            Assert.AreNotEqual(even.Client, newOrder.Client);
            Assert.AreNotEqual(even.ProductVariant, newOrder.ProductVariant);
            Assert.AreNotEqual(even.Quantity, newOrder.Quantity);

            Assert.AreEqual(rep.UpdateEventById(0, newOrder), true);

            Assert.AreNotEqual(even.Id, newOrder.Id);
            Assert.AreEqual(even.Client, newOrder.Client);
            Assert.AreEqual(even.ProductVariant, newOrder.ProductVariant);
            Assert.AreEqual(even.Quantity, newOrder.Quantity);
        }
        [TestMethod]
        public void EventUpdateById_ArgumentOutOfRange_Smaller()
        {
            DataRepository rep = new DataRepository(new StaticFiller());
            Product product = new Product(5, "Test", "Test", "Test");
            ProductVariant productVariant = new ProductVariant(5, product, 3.0, 30.0, "Test");
            Client client = new Client(5, "Test", "Test", "test@test.test", "00-000 Test, Testowa 0");
            Order order = new Order(5, productVariant, client, 5, DateTime.Now);
            Assert.AreEqual(false, rep.UpdateEventById(-1, order));
        }
        [TestMethod]
        public void EventUpdateById_ArgumentOutOfRange_Bigger()
        {
            DataRepository rep = new DataRepository(new StaticFiller());
            Product product = new Product(5, "Test", "Test", "Test");
            ProductVariant productVariant = new ProductVariant(5, product, 3.0, 30.0, "Test");
            Client client = new Client(5, "Test", "Test", "test@test.test", "00-000 Test, Testowa 0");
            Order order = new Order(5, productVariant, client, 5, DateTime.Now);
            Assert.AreEqual(false, rep.UpdateEventById(100, order));
        }
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void EventUpdateById_ArgumentNull()
        {
            DataRepository rep = new DataRepository(new StaticFiller());
            rep.UpdateEventById(0, null);
        }
        #endregion

        #region EventDeleteAtPosition
        [TestMethod]
        public void EventDeleteAtPosition()
        {
            DataRepository rep = new DataRepository(new StaticFiller());
            Assert.AreEqual(5, rep.GetAllEvents().Count);
            int idToDelete = rep.GetEventAtPosition(0).Id;
            rep.DeleteEventAtPosition(0);
            Assert.AreEqual(4, rep.GetAllEvents().Count);
            Assert.AreEqual(null, rep.GetEventById(idToDelete));
        }
        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void EventDeleteAtPosition_ArgumentOutOfRange_Smaller()
        {
            DataRepository rep = new DataRepository(new StaticFiller());
            rep.DeleteEventAtPosition(-1);
        }
        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void EventDeleteAtPosition_ArgumentOutOfRange_Bigger()
        {
            DataRepository rep = new DataRepository(new StaticFiller());
            rep.DeleteEventAtPosition(100);
        }
        #endregion

        #region EventDeleteById
        [TestMethod]
        public void EventDeleteById()
        {
            DataRepository rep = new DataRepository(new StaticFiller());
            Assert.AreEqual(5, rep.GetAllEvents().Count);
            Assert.AreEqual(true, rep.DeleteEventById(0));
            Assert.AreEqual(4, rep.GetAllEvents().Count);
            Assert.AreEqual(null, rep.GetEventById(0));
        }
        [TestMethod]
        public void EventDeleteById_ArgumentOutOfRange_Smaller()
        {
            DataRepository rep = new DataRepository(new StaticFiller());
            Assert.AreEqual(false, rep.DeleteEventById(-1));
        }
        [TestMethod]
        public void EventDeleteById_ArgumentOutOfRange_Bigger()
        {
            DataRepository rep = new DataRepository(new StaticFiller());
            Assert.AreEqual(false, rep.DeleteEventById(100));
        }
        #endregion

        #endregion
    }
}
